<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// prefix is for route and namespace is for controllers directory
// for guest room
Route::group(['namespace' => 'Guest'], function () {
    Route::resource('cpdGuest', 'RoomDashboardController');
    Route::resource('cpdGuestRoom', 'RoomController');
});
// for login 
Route::get('/', function () {
    return view('auth.login');
});

// for files download
Route::resource('files_download', 'FilesDownloadController');
// for main dashboard
Route::resource('main_dashboard', 'MainDashboardController');


///////////////////// FOR MEMBERS ROUTES //////////////////////////////// 
Route::group(['prefix' => 'Member','namespace' => 'Member'], function () {
    
    // for video conference
    Route::group(['namespace' => 'Conference'], function () {
        Route::resources([
            'conference_dashboard'=>'ConferenceDashboardController',
            'conference_room'=>'ConferenceRoomController',
        ]);
    });
    // for library
    Route::resources([
        'library_dashboard'=>'LibraryController',
        
    ]);
    // for messages
    Route::resources([
        'message_dashboard'=>'MessageController',
        
    ]);
});
// for trainees
Route::group(['prefix' => 'Trainee','namespace' => 'Trainee'], function () {
    Route::resources([
        'dashboard'=>'DashboardController',
        'course_materials'=>'CourseMatController',
        'registration'=>'RegistrationController',
        'fees'=>'FeeController',
        'lecturer_assessment'=>'LecturerAssessController',
        'trainee_conference'=>'ConferenceController',
        'results'=>'ResultController',
        'profile'=>'ProfileController',
    ]);

});

//////////////////////////////////////// FOR ADMIN ////////////////////////////
Route::group(['prefix' => 'Admin','namespace' => 'Admin'], function () {
    // for Members
    Route::group(['namespace' => 'Member'], function () {
        Route::resources([
            'member_dashboard'=>'DashboardController',
            'member_manage'=>'ManageController',
            'member_type'=>'MemberTypeController',
            'member_designation'=>'MemberDesignationController',
            'member_report'=>'ReportController',
            
        ]);
    });
    // for division
    Route::group(['namespace' => 'Division'], function () {
        Route::resources([
            'division_dashboard'=>'DashboardController',
            'division_manage'=>'ManageController',
            'division_account'=>'AccountController',
            'division_report'=>'ReportsController',
            
        ]);
    });
    // for users
    Route::group(['namespace' => 'Users'], function () {
        Route::resources([
            'users_dashboard'=>'DashboardController',
            'users_manage'=>'ManageController',
            'users_sessions'=>'SessionController',
            'users_report'=>'ReportController',
            
        ]);
    });
    // for modules
    Route::group(['namespace' => 'Module'], function () {
        Route::resources([
            'module_manage'=>'ManageController',

        ]);
    });
    // for groups
    Route::group(['namespace' => 'Groups'], function () {
        Route::resources([
            'group_manage'=>'ManageController',

        ]);
    });
    // for conference setup
    Route::group(['namespace' => 'VideoConference'], function () {
        Route::resources([
            'conference_manage'=>'ManageController',

        ]);
    });
    // for sms
    Route::group(['namespace' => 'Sms'], function () {
        Route::resources([
            'sms_manage'=>'ManageController',

        ]);
    });
    // for trainees lecturers
    Route::group(['namespace' => 'TraineeLecturer'], function () {
        Route::resources([
            'lecturer_dashboard'=>'DashboardController',
            'lecturer_all_trainees'=>'AllTraineeController',
            'lecturer_course_material'=>'CourseMaterialController',
            'lecturer_attendance'=>'ConferenceAttendanceController',
            'lecturer_exams_result'=>'ResultController',
            
        ]);
    });
    // for trainee moderator
    Route::group(['namespace' => 'TraineeModerator'], function () {
        Route::resources([
            'moderator_dashboard'=>'DashboardController',
            'moderator_setup_course'=>'SetupCourseController',
            'moderator_publish_result'=>'PublishResultController',
            'moderator_video_conference'=>'VideoConferenceController',
            'moderator_manage_trainee'=>'ManageTraineeController',
            'moderator_setup_calendar'=>'SetupCalendarController',
            'moderator_lecturer_assement'=>'ManagelecturerAssementController',
            
        ]);
    });

});
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


