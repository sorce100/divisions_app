@extends('layouts.header')
@section('pageTitle','Dashboard')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <!-- <h4 class="page-title">Dashboard1</h4>  -->
      <h4 class="text-center">{{ session()->get('member_division_name') }}</h4>
      
      <div class="clearfix"></div>
   </div>
   <!--End Page Title--> 
   <div class="page-title-box">
      <h4 class="page-title">Division Dashboard</h4> 
      <!-- {{ session()->get('member_division_name') }} -->
      <ol class="breadcrumb">
         <li><a href="dashboard.php">Dashboard</a></li>
         <li class="active">Dashboard</li>
      </ol>
      <div class="clearfix"></div>
   </div>       
   <!--Start row-->
   <div class="row">
      <!--Start info box-->
      <div class="col-md-3 col-sm-6">
         <div class="info-box-main">
            <div class="info-stats">
               <p>1250</p>
               <span>TOTAL MEMBERS </span>
            </div>
            <div class="info-icon text-primary ">
               <i class="fa fa-users"></i>
            </div>
            <div class="info-box-progress">
               <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--End info box-->
      <!--Start info box-->
      <div class="col-md-3 col-sm-6">
         <div class="info-box-main">
            <div class="info-stats">
               <p>2300</p>
               <span>UPCOMING CPDS</span>
            </div>
            <div class="info-icon text-primary">
               <i class="fa fa-dollar"></i>
            </div>
            <div class="info-box-progress">
               <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--End info box-->
      <!--Start info box-->
      <div class="col-md-3 col-sm-6">
         <div class="info-box-main">
            <div class="info-stats">
               <p>5320</p>
               <span>LIVE STREAM</span>
            </div>
            <div class="info-icon text-primary">
               <i class="fa fa-video-camera"></i>
            </div>
            <div class="info-box-progress">
               <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--End info box-->
      <!--Start info box-->
      <div class="col-md-3 col-sm-6">
         <div class="info-box-main">
            <div class="info-stats">
               <p>65</p>
               <span>TOTAL NEWS</span>
            </div>
            <div class="info-icon text-primary">
               <i class="fa fa-newspaper-o"></i>
            </div>
            <div class="info-box-progress">
               <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--End info box-->
   </div>
   <!--End summary tab row-->

   <!-- for shorttab -->
  

   <!-- end of shorttab -->

   <!--Start row-->
<div class="row">
   <!-- Start inbox widget-->
   <div class="col-md-6">
      <div class="white-box">
         <h2 class="header-title">Latest News</h2>
         <hr>
         <!-- .inbox-scroll-list -->
         <div class="inbox-widget">
            <div class="inbox-inner">
               <a href="#">
                  <div class="inbox-item">
                     <div class="inbox-img">
                        <img src="../assets/images/news-icon.jpg" alt="News Image" />
                     </div>
                     <div class="inbox-item-info">
                        <p class="author">Gov’t to reopen Ghana School of Surveying and Mapping</p>
                        <p class="inbox-message">Read More....</p>
                        <!-- <p class="inbox-date">13:34 PM</p> -->
                     </div>
                  </div>
               </a>
            </div>
            <div class="inbox-inner">
               <a href="#">
                  <div class="inbox-item">
                     <div class="inbox-img">
                        <img src="../assets/images/news-icon.jpg" alt="News Image" />
                     </div>
                     <div class="inbox-item-info">
                        <p class="author">Gov’t to reopen Ghana School of Surveying and Mapping</p>
                        <p class="inbox-message">Read More....</p>
                        <!-- <p class="inbox-date">13:34 PM</p> -->
                     </div>
                  </div>
               </a>
            </div>
            <div class="inbox-inner">
               <a href="#">
                  <div class="inbox-item">
                     <div class="inbox-img">
                        <img src="../assets/images/news-icon.jpg" alt="News Image" />
                     </div>
                     <div class="inbox-item-info">
                        <p class="author">Gov’t to reopen Ghana School of Surveying and Mapping</p>
                        <p class="inbox-message">Read More....</p>
                        <!-- <p class="inbox-date">13:34 PM</p> -->
                     </div>
                  </div>
               </a>
            </div>
            <div class="inbox-inner">
               <a href="#">
                  <div class="inbox-item">
                     <div class="inbox-img">
                        <img src="../assets/images/news-icon.jpg" alt="News Image" />
                     </div>
                     <div class="inbox-item-info">
                        <p class="author">Gov’t to reopen Ghana School of Surveying and Mapping</p>
                        <p class="inbox-message">Read More....</p>
                        <!-- <p class="inbox-date">13:34 PM</p> -->
                     </div>
                  </div>
               </a>
            </div>
            <div class="inbox-inner">
               <a href="#">
                  <div class="inbox-item">
                     <div class="inbox-img">
                        <img src="../assets/images/news-icon.jpg" alt="News Image" />
                     </div>
                     <div class="inbox-item-info">
                        <p class="author">Gov’t to reopen Ghana School of Surveying and Mapping</p>
                        <p class="inbox-message">Read More....</p>
                        <!-- <p class="inbox-date">13:34 PM</p> -->
                     </div>
                  </div>
               </a>
            </div>
         </div>
      </div>
   </div>
   <!-- End inbox widget-->

   <!-- Start timeline-->
   <div class="col-md-6">
    <div class="white-box">
       <h2 class="header-title">User Latest Activity</h2>
        <hr>
       <ul class="timeline timeline-compact">
          <li class="latest">
             <div class="timeline-date">Just Now</div>
             <div class="timeline-title">Create New Page</div>
             <div class="timeline-description">Lorem Ipsum generators on the tend to repeat.</div>
          </li>
          <li>
             <div class="timeline-date">Just Now</div>
             <div class="timeline-title">Back Up Theme</div>
             <div class="timeline-description">Lorem Ipsum generators on the tend to repeat.</div>
          </li>
          <li>
             <div class="timeline-date">Just Now</div>
             <div class="timeline-title">Changes In The Structure</div>
             <div class="timeline-description">Lorem Ipsum generators on the tend to repeat. </div>
          </li>
          <li>
             <div class="timeline-date">Just Now</div>
             <div class="timeline-title">New user register</div>
             <div class="timeline-description">Lorem Ipsum generators on the tend to repeat.</div>
          </li>
          <li>
             <div class="timeline-date">Just Now</div>
             <div class="timeline-title">New user register</div>
             <div class="timeline-description">Lorem Ipsum generators on the tend to repeat.</div>
          </li>
          <li>
             <div class="timeline-date">Just Now</div>
             <div class="timeline-title">New user register</div>
             <div class="timeline-description">Lorem Ipsum generators on the tend to repeat.</div>
          </li>
       </ul>
    </div>
  </div>
   <!-- End timeline-->  
  
</div>
<!-- for calendar -->
<div class="row">
  <div class="calendar-layout clearfix">
     <!-- Start To Do List-->   
     <div class="col-md-6">
       <div class=" white-box">
           <h2 class="header-title">User To Do List 
            
          </h2>
           <hr>
            <ul class="todo-list list-task ">
               <li>
                  <div class="checkbox checkbox-primary">
                    <input type="checkbox"  id="todolist">
                    <label for="todolist">Schedule meeting </label>
                  </div>  
              </li>
              
               
            </ul>                      
       </div>
     </div>      
    <!-- End To Do List-->   
    <div class="col-md-6">
        <div class="white-box">
          <h2 class="header-title">Upcoming Events</h2>
          <hr>
          <div id='calendar'></div>
      </div>
    </div>
  </div>      
</div>


<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
