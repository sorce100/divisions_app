@extends('layouts.header')
@section('pageTitle','Video Conference')
@section('pageBody')
<div class="wrapper">
   
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Class Room</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
            <div id="jitsi_meet_div"></div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endSection

@section('pageScript')
<script src="https://meet.jit.si/external_api.js"></script>
<script type="text/javascript">
	const domain = "lisagonline.info";
	const options = {
	  roomName: "{{$conferenceDetails}}",
	  width: '100%',
	  height: 750,
	  parentNode: document.querySelector("#jitsi_meet_div"),
	};
	const api = new JitsiMeetExternalAPI(domain, options);
</script>
@endSection
