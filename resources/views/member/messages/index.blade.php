@extends('layouts.header')
@section('pageTitle','Messages')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">User Messages</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
   <div class="col-md-12">
      <div class="row">
         <div class="col-md-3">
            <div class="white-box">
              <a href="compose.html" class="btn btn-primary outline-btn btn-block m-b-10" data-toggle="modal" data-target="#composeModal">Compose</a>
              <ul class="list-unstyled mailbox-nav">
                 <li class="active"><a href="inbox.html"><i class="fa fa-inbox"></i>Inbox 
                    <span class="badge badge-success">4</span></a>
                 </li>
                 <li><a href="#"><i class="fa fa-sign-out"></i>Sent</a></li>
                 <li><a href="#"><i class="fa fa-file-text-o"></i>Draft</a></li>
              </ul>
            </div>
         </div>
         <div class="col-md-9">
            <div class="white-box">
               <div class="mailbox-content">
                  <table class="table">
                     <thead>
                        <tr>
                           <th colspan="1" class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelectAll">
                                 <label></label>
                              </div>
                           </th>
                           <th class="text-right" colspan="5">
                              <a class="btn btn-primary outline-btn m-r-10"  title="Refresh"><i class="fa fa-refresh"></i></a>
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star icon-state-warning"></i>
                           </td>
                           <td class="hidden-xs">
                              Jacob
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                           </td>
                           <td>
                              21 march
                           </td>
                        </tr>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star icon-state-warning"></i>
                           </td>
                           <td class="hidden-xs">
                              Mark Doe
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              20 march
                           </td>
                        </tr>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star icon-state-warning"></i>
                           </td>
                           <td class="hidden-xs">
                              Adobe
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                              <i class="fa fa-paperclip"></i>
                           </td>
                           <td>
                              18 march
                           </td>
                        </tr>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star icon-state-warning"></i>
                           </td>
                           <td class="hidden-xs">
                              Apple
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                              <i class="fa fa-paperclip"></i>
                           </td>
                           <td>
                              16 Oct
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Marry
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                              <i class="fa fa-paperclip"></i>
                           </td>
                           <td>
                              12 June
                           </td>
                        </tr>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Marry
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              12 June
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Marry
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Marry
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star icon-state-warning"></i>
                           </td>
                           <td class="hidden-xs">
                              Marry
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star icon-state-warning"></i>
                           </td>
                           <td class="hidden-xs">
                              Mark Doe
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Mark Doe
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="unread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Mark Doe
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Zorita Serrano
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="read">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Zorita Serrano
                           </td>
                           <td>
                              There are many variations of passages of Lorem Ipsum 
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>
                        <tr class="uread">
                           <td class="hidden-xs">
                              <div class="checkbox">
                                 <input type="checkbox" class="checkBoxSelect" >
                                 <label></label>
                              </div>
                           </td>
                           <td class="hidden-xs">
                              <i class="fa fa-star"></i>
                           </td>
                           <td class="hidden-xs">
                              Williams
                           </td>
                           <td>
                              Lorem Ipsum is simply dummy text of the and typesetting
                           </td>
                           <td>
                           </td>
                           <td>
                              20 April
                           </td>
                        </tr>

                     </tbody>
                  </table>
               </div>
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-7"> Showing 1 - 15 of 200 </div>
                     <div class="col-md-5">
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-primary outline-btn m-r-5"><i class="fa fa-angle-left"></i></button>
                           <button type="button" class="btn btn-primary outline-btn"><i class="fa fa-angle-right"></i></button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
<script src="{{ asset('assets/pages/inbox.js') }}"></script>
<script src="{{ asset('assets/plugins/summernote-master/summernote.min.js') }}"></script>
<script src="{{ asset('assets/pages/compose.js') }}"></script>



<!-- modal for Composing message -->
<div  id="composeModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Compose New Message</h4>
         </div>
         <div class="modal-body">
          <form>
            <div class="row">
              <div class="form-group">
                <label for="" class="col-md-2 control-label"></label>
                <div class="col-md-10">
                  <label class="checkbox-inline"><input type="radio" name="message_send_status" value="send" checked> Send Message</label>
                  <label class="checkbox-inline"><input type="radio" name="message_send_status" value="draft"> Draft Message</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <label for="to" class="col-md-2 control-label">To <span class="asterick">*</span></label>
                <div class="col-md-10">
                  <input type="text" class="form-control" id="to">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <label for="subject" class="col-md-2 control-label">Subject <span class="asterick">*</span></label>
                <div class="col-md-10">
                  <input type="text" class="form-control" id="subject">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="compose-message">
                  <div class="summernote"></div>
                </div>
              </div>
            </div>
          <!--end modal contnet -->
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-trash"></i> Discard</button>
            <button type="button" class="btn btn-primary">Send Message <i class="fa fa-send"></i></button>
         </div>
        </form>
      </div>
   </div>
</div>
@endsection
