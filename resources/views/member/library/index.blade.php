@extends('layouts.header')
@section('pageTitle','Library')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Library Archive</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">Division Archive</h2>
          <hr>
            <!-- page Content goes in here -->
	         <!-- tabs for nvaigation -->

	          <div class="panel panelTabs" >
	            <ul class="nav nav-tabs nav-justified">
	              <li class="active"><a data-toggle="tab" href="#cpd">CPD <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#monthmeeting">Monthly Meetings <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#seminar">Seminar <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#ysn">Young Surveyors  <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#library">Library <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#publication">Publications <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#conference">Conference <i class="fa fa-paste"></i></a></li>
	              <li><a data-toggle="tab" href="#committee">Committee <i class="fa fa-paste"></i></a></li>
	            </ul>
	          </div>

	          <!-- tab content -->
	          <div class="tab-content">
                  <div id="cpd" class="tab-pane fade in active">
                    <div class="table-responsive">
                        <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="cpdDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="monthmeeting" class="tab-pane fade">
                    <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="monthmeetingDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="seminar" class="tab-pane fade">
                    <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="seminarDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="ysn" class="tab-pane fade">
                   <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="ysnDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="library" class="tab-pane fade">
                   <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="libraryDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="publication" class="tab-pane fade">
                    <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="publicationDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="conference" class="tab-pane fade">
                    <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="conferenceDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <div id="committee" class="tab-pane fade">
                    <div class="table-responsive"><br>
                       <table class="tableInit display table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ARCHIVE SUBJECT</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="committeeDisplay">
                                  
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
	         <!-- End of page content -->
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endSection

@section('pageScript')

@endsection