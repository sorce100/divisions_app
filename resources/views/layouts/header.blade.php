<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="{{ session()->get('member_division_logo') }}" type="image/png">
      <title>@yield('pageTitle')</title>
      <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
      <!-- full calendaer -->
      <link href="{{ asset('assets/plugins/full-calendar/fullcalendar.css') }}" rel="stylesheet" type="text/css" />
      <!-- summernote -->
      <link href="{{ asset('assets/plugins/summernote-master/summernote.css') }}" rel="stylesheet" type="text/css"/>
      <!-- for datepicker -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datetimepicker/datetimepicker.min.css') }}" />
      <!-- for select 2 -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/select2/select2.min.css') }}" />
      <!-- for parsley -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/parsley/parsley.css') }}" />
      <!-- for roastr -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}" />
      <!-- for datatables -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}" />
      <!-- sweet alert -->
      <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
         
      @yield('pageStyle')

   </head>
   <body class="sticky-header">
      <!--Start left side Menu-->
      <div class="left-side sticky-left-side">
         <!--logo-->
         <div class="mainLogo">
            <a href="{{ route('main_dashboard.index') }}">
              <img class="img img-circle mainLogo_img" src="{{ session()->get('member_division_logo') }}" alt="" >
            </a>
         </div>
         <!--logo-->
         <div class="left-side-inner">
            <!--Sidebar nav-->
            <ul class="nav nav-pills nav-stacked custom-nav">
               <!-- dashboard -->
               <li><a href="{{ route('main_dashboard.index') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
               <!-- for looping through group modals ids -->
               @if(session()->get("member_modules_urls") !== 'null' || !empty(session()->get("member_modules_urls")))   
                  @for ($i=0; $i < sizeof(session()->get("member_modules_urls")); $i++) 
                     @php
                        echo session()->get("member_modules_urls")[$i]->module_url;
                     @endphp
                  @endfor
               @endif     
               <!-- please finish this tomorrow -->

               <li><a href="{{ route('profile.index') }}"><i class="fa fa-pencil-square-o"></i> <span>CPD Registration</span></a></li>
               
 
               
               <!-- cpd register -->
               <!-- <li><a href="{{ route('conference_dashboard.index') }}"><i class="fa fa-users"></i> <span>CPD Register</span></a></li>
               <li><a href="{{ route('conference_dashboard.index') }}"><i class="fa fa-th-large"></i> <span>Committees</span></a></li>
               <li><a href="{{ route('conference_dashboard.index') }}"><i class="fa fa-comments"></i> <span>Broadcast SMS</span></a></li>
               <li class="menu-list">
                  <a href="#"><i class="fa fa-credit-card"></i> <span>Payments</span></a>
                  <ul class="sub-menu-list">
                     <li><a href="{{ route('dashboard.index') }}"> Annual Subscription </a></li>
                     <li><a href="{{ route('dashboard.index') }}"> Voluntary Contribution </a></li>
                  </ul>
               </li>
               <li><a href="{{ route('conference_dashboard.index') }}"><i class="fa fa-newspaper-o"></i> <span>News</span></a></li>
               <li><a href="{{ route('conference_dashboard.index') }}"><i class="fa fa-history"></i> <span>Wallet History</span></a></li> -->
               


            
               
            </ul>
            <!--End sidebar nav-->
         </div>
      </div>
      <!--End left side menu-->
      <!-- main content start-->
      <div class="main-content" >
      <!-- header section start-->
      <div class="header-section">
         <a class="toggle-btn"><i class="fa fa-bars"></i></a>
         <div class="searchform">
             <input type="text" readonly value="
             Balance (₵) : 500.00
             " />
         </div>
         <!--notification menu start -->
         <div class="menu-right">
            <ul class="notification-menu">
               <li>
                  <!-- <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                  <i class="fa fa-envelope"></i>
                  <span class="badge">8</span>
                  </a> -->
                  <div class="dropdown-menu dropdown-menu-head pull-right">
                     <h5 class="title">Messages</h5>
                     <ul class="dropdown-list">
                        <li class="notification-scroll-list notification-list ">
                           <!-- list item-->
                           <a href="javascript:void(0);" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left p-r-10">
                                    <em class="fa  fa-shopping-cart noti-primary"></em>
                                 </div>
                                 <div class="media-body">
                                    <h5 class="media-heading">A new order has been placed.</h5>
                                    <p class="m-0">
                                       <small>29 min ago</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- list item-->
                           <a href="javascript:void(0);" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left p-r-10">
                                    <em class="fa fa-check noti-success"></em>
                                 </div>
                                 <div class="media-body">
                                    <h5 class="media-heading">Databse backup is complete</h5>
                                    <p class="m-0">
                                       <small>12 min ago</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- select 5 -->
                        </li>
                        <li class="last"> <a href="#">View all Messages</a> </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <!-- <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="badge">4</span>
                  </a> -->
                  <div class="dropdown-menu dropdown-menu-head pull-right">
                     <h5 class="title">Notifications</h5>
                     <ul class="dropdown-list normal-list">
                        <li class="message-list message-scroll-list">
                           <a href="#">
                             <span class="photo"> <img src="{{ asset('assets/images/users/avatar-8.jpg') }}" class="img-circle" alt="img"></span>
                             <span class="subject">John Doe</span>
                             <span class="message"> New tasks needs to be done</span>
                             <span class="time">15 minutes ago</span>
                           </a>
                           <a href="#">
                             <span class="photo"> <img src="{{ asset('assets/images/users/avatar-7.jpg') }}" class="img-circle" alt="img"></span>
                             <span class="subject">John Doe</span>
                             <span class="message"> New tasks needs to be done</span>
                             <span class="time">10 minutes ago</span>
                           </a>
                            <!-- select 5 -->
                        </li>
                        <li class="last"> <a  href="#">View all notifications</a> </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ session()->get('member_image') }}" alt="" />
                  {{ session()->get('member_fullName') }}
                  <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                     <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                     <li> <a href="#"> <i class="fa fa-info"></i> Help </a> </li>
                     <li>  
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-lock"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                        </form>
                     </li>
                  </ul>
               </li>
            </ul>
         </div>
         <!--notification menu end -->
      </div>
      <!-- header section end-->

      @yield('pageBody')

      <!--Start  Footer -->
      <footer class="footer-main"> {{date('Y')}}  &copy; <a target="_blank" href="http://theprismoid.com/">Prismoidal Company Limited</a>  </footer>
      <!--End footer -->
      </div>
      <!--End main content -->
      <!--Begin core plugin -->
      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
      <script  src="{{ asset('assets/js/jquery.slimscroll.js ') }}"></script>
      <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
      <script src="{{ asset('assets/js/functions.js') }}"></script>
      <!-- End core plugin -->

      <!--Begin Page Level Plugin-->
      <script src="{{ asset('assets/pages/dashboard.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/plugins/full-calendar/fullcalendar.js') }}"></script>
      <!-- for calendar -->
      <script type="text/javascript" src="{{ asset('assets/pages/calendar-custom.js') }}"></script>
      <!-- for datepicker -->
      <script type="text/javascript" src="{{ asset('assets/plugins/datetimepicker/datetimepicker.min.js') }}" ></script>
      <!-- for select 2 -->
      <script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }}" ></script>
      <!-- for parsley -->
      <script type="text/javascript" src="{{ asset('assets/plugins/parsley/parsley.min.js') }}" ></script>
      <!-- for roastr -->
      <script type="text/javascript" src="{{ asset('assets/plugins/toastr/toastr.min.js') }}" ></script>
      <!-- datatables -->
      <script type="text/javascript" src="{{ asset('assets/plugins/datatables/datatables.min.js') }}" ></script>

      <!--  -->

      @yield('pageScript')
      <!--End Page Level Plugin-->
      </body>
      </html>
      
      <script>
         // <!-- for checking all checkboxes -->
         $('#checkAll').click(function () {    
            $('input:checkbox').prop('checked', this.checked);    
         });
         // for datepicker
         $(function() {
            $('[data-toggle="datepicker"]').datetimepicker({
              language: 'en-GB',
              format: 'dd-mm-yyyy | hh:ii P',
              autoclose: true,
              zIndex: 2048,
            });
            // for date only
            $('[data-toggle="dateOnlypicker"]').datetimepicker({
               minView: 2,
               language: 'en-GB',
               format: 'dd-mm-yyyy',
               autoclose: true,
               zIndex: 2048,
            });
            // data-toggle="dateOnlypicker"
          });
      </script>
      

   <!-- for pssword change -->
   <div class="modal fade" id="changePassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header" id="bg">
           <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default btnClose">&times; CLOSE</span></button>
          <h4 class="modal-title"><b id="subject">Change Password</b></h4>
        </div>
        <div class="modal-body" id="bg">
          <form id="change_password_form" method="POST"> 
              <!--  -->
            <div class="row">
              <div class="col-md-4"><label>New Password <span class="asterick"> *</span></label></div>
              <div class="col-md-8">
                <input type="password" class="form-control" id="newPasswd" name="newPasswd" minlength="4" autocomplete="off" placeholder="Enter New Password &hellip;" required>
              </div>
            </div>

            <br>
            <!--  -->
            <div class="row">
              <div class="col-md-4"><label>Retype Password <span class="asterick"> *</span></label></div>
              <div class="col-md-8">
                <input type="password" class="form-control" id="retypeNewPasswd" name="retypeNewPasswd" minlength="4" autocomplete="off" placeholder="Retype Password &hellip;" required>
              </div>
            </div>

              <br>
              <input type="hidden" name="mode" value="userChangePassword">

              <div class=" modal-footer" id="bg">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                <button type="submit" class="btn btn-info" id="changePass_btn">Change Password <i class="fa fa-exchange"></i></button>
              </div>        
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

