<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="{{ asset('assets/images/logo2.png') }}" type="image/png">
      <title>Prismoidal Company Limited</title>
      <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
      <!-- toast -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
   </head>
   <body class="sticky-header">
      <!--Start login Section-->
      <section class="login-section">
         <div class="container">
            <div class="row">
               <div class="login-wrapper">
                  <div class="login-inner">
                     <div class="logo">
                        <img src="{{ asset('assets/images/logo2.png') }}"  alt="logo"/>
                     </div>
                     <br>
                     <h2 class="header-title text-center">Login</h2>
                     <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                           <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->
                           
                              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter User Email">
                              @error('email')
                              <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                           
                        </div>
                        <div class="form-group">
                           <!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> -->
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter User Password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            @if (Route::has('password.request'))
                            <a class="btn btn-link pull-right mb-10" href="{{ route('password.request') }}"><b>{{ __('Forgot Your Password?') }} <i class="fa fa-lock"></i></b></a>
                            @endif
                            <br>
                            <button type="submit" class="btn btn-primary btn-block mb-5"><b>{{ __('Login') }} <i class="fa fa-sign-in"></i></b></button>
                            <hr>
                            <div class="form-group text-center">
                                Don't have a Trainee account?  <a href="#" style="color: red;">Register </a>
                            </div>
                        </div>
                     </form>
                     <div class="copy-text">
                        <p class="m-0"><?php echo date('Y'); ?> &copy; <a target="_blank" href="http://theprismoid.com/">Prismoidal Company Limited</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--End login Section-->
      <!--Begin core plugin -->
      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
      <!-- End core plugin -->
      <!-- mcafee antivirus -->
      <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
   </body>
</html>