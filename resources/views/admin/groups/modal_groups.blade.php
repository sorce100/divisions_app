<div  id="addGroup" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="groupTitle">Add New Group</h4>
         </div>
         <form method="POST" class="form-horizontal" id="group_form" >
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-2"><label>Group Division</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <select name="group_division_id" id="group_division_id" class="form-control" required>
                           <option selected disabled>Please Select</option>
                           @foreach($divisions as $division)
                           <option value="{{$division->id}}">{{$division->division_alias}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Group Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="text" class="form-control" id="group_name" name="group_name" autocomplete="off" placeholder="Enter Group Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Select Modules</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <div class="table-responsive">
                           <table class="table table-striped table-bordered">
                              <thead class="thead_bg">
                                 <tr>
                                    <th><input type="checkbox" id="checkAll" ></th>
                                    <th>Module Name</th>
                                    <th>Module Category</th>
                                    <th>Module Division</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($modules as $module)
                                 <tr>
                                    <td>
                                       <input type="checkbox" name="group_module_id[]" class="group_module_id" value="{{ $module->id }}" >
                                    </td>
                                    <td>{{ $module->module_name }}</td>
                                    <td>{{ $module->module_category }}</td>
                                    <td>{{ $module->division_alias }}</td>
                                 </tr>
                                 @endforeach
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <!--  -->
               <input type="hidden" name="group_id" id="group_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="group_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>
    $(document).ready(function() {
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#group_form').parsley();
      // on click
      $('#addGroupBtn').click(function () {
         $('#group_form').parsley().reset();
         $('#group_id').val('');
         $('#group_form').trigger("reset");
         // clear all checkboxes
         $('#groupTitle').html("Add New Group");
         $('#actionType').val("create");
         $('#group_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="group_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
         $('#addGroup').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editGroup', function () {
         // reset from on click edit button
         $('#group_form').trigger("reset");
         // 
        var data_id = $(this).data('id');
        var url = '{{ route("group_manage.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
         // check if not null
         if(data.group_module_id !== 'null'){
            // passing the group pages array stored in database
            var groupPagesArray = JSON.parse(data.group_module_id);
            $('input[class = "group_module_id"]').each(function(){ 
               var checkBoxValue = $(this).prop('value');
               // check if value is incldued in array
               if (groupPagesArray.includes(checkBoxValue)) {
                  $(this).prop('checked',true);
               }
            });
         }
         
         $('#groupTitle').html("Edit Group");
         $('#group_id').val(data.id);
         $('#group_division_id').val(data.group_division_id).trigger('change');
         $('#group_name').val(data.group_name);
         $('#actionType').val("edit");
         $('#group_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="group_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
         $('#addGroup').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteGroup', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("group_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_group').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#group_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#group_form').serialize(),
          url: "{{ route('group_manage.store') }}",
          type: "POST",
          success: function (data) {
              switch (data.message) {
                case 'successful':
                  $('#addGroup').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_group').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
            toastr.error('Sorry! error');
            var errormsg = JSON.parse(data.responseText);
            // loop through the 
            swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });


  </script>