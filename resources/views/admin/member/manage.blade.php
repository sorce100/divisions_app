@extends('layouts.header')
@section('pageTitle','Manage Members')
@section('pageStyle')
<!-- for dropify -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}" />
@endsection
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Manage Division Members</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
          <button class="btn btn-primary" id="addMemberBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
          <table id="table_member" class="table display" style="width:100%">
            <thead>
              <th>ID</th>
              <th>Member Image</th>
              <th>Member Name</th>
              <th>Member Email</th>
              <th>Member Tel</th>
              <th>Member Prof Number</th>
              <th>Member Region</th>
              <th>Updated</th>
              <th>Action</th>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  <script type="text/javascript" src="{{ asset('assets/plugins/dropify/js/dropify.js') }}" ></script>
  <!-- modal include -->
  @include('admin.member.modal_member')
  <!-- for file viewer -->
  @include('files_access.file_viewer')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_member').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('member_manage.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id', name: 'id', 'visible': false},
                      { data: 'memberImage'},
                      { data: 'member_full_name'},
                      { data: 'member_email'},
                      { data: 'member_tel'},
                      { data: 'member_professional_number'},
                      { data: 'member_region'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

<!-- modal for adding new task -->
