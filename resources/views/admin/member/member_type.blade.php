@extends('layouts.header')
@section('pageTitle','Manage Member Type')
@section('pageStyle')
@endsection
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Member Types</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
            <button class="btn btn-primary" id="addMemberTypeBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
            <table id="table_member_type" class="table display" style="width:100%">
            <thead>
              <th>ID</th>
              <th>Member Type Name</th>
              <th>Updated</th>
              <th>Action</th>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  <!-- modal include -->
  @include('admin.member.modal_member_type')

  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_member_type').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('member_type.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id', name: 'id', 'visible': false},
                      { data: 'member_type_name'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection
