<div  id="addMemberType" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="memberTypeTitle">New Member Type</h4>
         </div>
         <form method="POST"  class="form-horizontal" id="member_type_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-3"><label>Member Type Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" id="member_type_name" name="member_type_name" autocomplete="off" placeholder="Enter Member Type Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Member Type Notes</label> </div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <textarea name="member_type_notes" id="member_type_notes" rows="5" class="form-control" placeholder="Enter Member Type Notes &hellip;" >None</textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <input type="hidden" name="member_type_id" id="member_type_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="member_type_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>
   $(document).ready(function() {
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#member_type_form').parsley();
      // on click
      $('#addMemberTypeBtn').click(function () {
          $('#member_type_form').parsley().reset();
          $('#member_type_id').val('');
          $('#member_type_form').trigger("reset");
          $('#memberTypeTitle').html("New Member Type");
          $('#actionType').val("create");
          $('#member_type_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="member_type_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
          $('#addMemberType').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editMemberType', function () {
        $('#member_type_form').trigger("reset");
        var data_id = $(this).data('id');
        var url = '{{ route("member_type.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
          $('#memberTypeTitle').html("Edit Member Type");
          $('#member_type_id').val(data.id);
          $('#member_type_name').val(data.member_type_name);
          $('#member_type_notes').html(data.member_type_notes);
          $('#actionType').val("edit");
          $('#member_type_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="member_type_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
          $('#addMemberType').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteMemberType', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("member_type.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_member_type').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#member_type_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#member_type_form').serialize(),
          url: "{{ route('member_type.store') }}",
          type: "POST",
          success: function (data) {
            //  console.log(data);
              switch (data.message) {
                case 'successful':
                  $('#addMemberType').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_member_type').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });

</script>