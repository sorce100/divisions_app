<div  id="addMember" class="modal fade" role="dialog">
   <div class="modal-dialog modal-xl">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="memberTitle">Add New Member</h4>
         </div>
         <form enctype="multipart/form-data" method="POST" class="form-horizontal" id="member_form">
            <div class="modal-body">
                <!-- modal contnet -->
                <!-- first field set -->
                <fieldset>
                    <legend>STEP 1 : Personal Details</legend>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Name</label> <span class="asterick">*</span></div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_first_name" name="member_first_name" autocomplete="off" placeholder="Enter Member First Name &hellip;" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_middle_name" name="member_middle_name" autocomplete="off" placeholder="Enter Member Middle Name &hellip;" >
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_last_name" name="member_last_name" autocomplete="off" placeholder="Enter Member Last Name &hellip;" required>
                            </div>
                        </div>
                    </div>
                    
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member DOB</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_dob" data-toggle="dateOnlypicker" name="member_dob" autocomplete="off" placeholder="click to select date" readonly >
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Tel / Emergency Contact</label> <span class="asterick">*</span></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="number" class="form-control" id="member_tel" name="member_tel" autocomplete="off" placeholder="xxx xxx xxxx &hellip;" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="number" class="form-control" id="member_emergency_tel" name="member_emergency_tel" autocomplete="off" placeholder="xxx xxx xxxx &hellip;" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Email</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="email" class="form-control" id="member_email" name="member_email" autocomplete="off" placeholder="example@abc.com &hellip;" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Region</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select name="member_region" id="member_region" class="form-control">
                                    <option selected disabled> Please Select </option>
                                    @include('regions')
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-3"><label>Member Image</label> <span class="asterick">*</span></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="file" name="member_image" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="jpg png jpeg">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <div id="member_image_div"></div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Address</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea name="member_address" id="member_address" rows="4" class="form-control" placeholder="Enter Member Address &hellip;" required> </textarea>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member House Location</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea name="member_location" id="member_location" rows="4" class="form-control" placeholder="Enter Member Location &hellip;" required> </textarea>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </fieldset>
                <!-- fieldset 2 -->
                <fieldset>
                    <legend>STEP 2 : MEMBER DIVISION DETAILS</legend>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Professional Number</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="number" class="form-control" id="member_professional_number" name="member_professional_number" autocomplete="off" placeholder="xxx xxx xxxx &hellip;" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Type</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select name="member_type" id="member_type" class="form-control">
                                    <option selected disabled> Please Select </option>
                                    @foreach($memberTypes as $type)
                                    <option value="{{$type->id}}">{{$type->member_type_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Designation</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select name="member_designation_id" id="member_designation_id" class="form-control">
                                    <option selected disabled> Please Select </option>
                                    @foreach($memberDesignations as $designation)
                                    <option value="{{$designation->id}}">{{$designation->member_designation_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </fieldset>
                <!-- fieldset 3 -->
                <fieldset>
                    <legend>STEP 3 : MEMBER COMPANY DETAILS</legend>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Company Name</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_company_name" name="member_company_name" autocomplete="off" placeholder="xxx xxx xxxx &hellip;" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Company Type</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select name="member_company_type" id="member_company_type" class="form-control">
                                    <option selected disabled> Please Select </option>
                                    <option value="public_service">Public Service</option>
                                    <option value="civil_service">Civil Service</option>
                                    <option value="private_service">Private Service</option>
                                    <option value="self_employed">Self Employed</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Company Tel</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="number" class="form-control" id="member_company_tel" name="member_company_tel" autocomplete="off" placeholder="xxx xxx xxxx &hellip;" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Company Email</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="email" class="form-control" id="member_company_email" name="member_company_email" autocomplete="off" placeholder="example@abc.com &hellip;" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Company Region</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select name="member_company_region" id="member_company_region" class="form-control">
                                    <option selected disabled> Please Select </option>
                                    @include('regions')
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Office Address</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea name="member_office_address" id="member_office_address" rows="4" class="form-control" placeholder="Enter Member Office Address &hellip;" required> </textarea>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Office Location</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea name="member_office_location" id="member_office_location" rows="4" class="form-control" placeholder="Enter Member office Location &hellip;" required> </textarea>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <hr>
                    <!--  -->
                    <div class="row m-b-10">
                        <div class="col-md-3"><label>Member Notes</label></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea name="member_notes" id="member_notes" rows="4" class="form-control" placeholder="Enter Member notes &hellip;"> NONE </textarea>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </fieldset>
                <!--  -->
                <input type="hidden" name="old_member_image" id="old_member_image" value="">
                <input type="hidden" name="member_folder" id="member_folder" value="">
                <input type="hidden" name="member_id" id="member_id" value="">
                <input type="hidden" name="actionType" id="actionType" value="create">
                <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                <!--end modal contnet -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-primary" id="member_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
        </form>
      </div>
   </div>
</div>

<script>
   $(document).ready(function(){
        // declare dropify
        $('.dropify').dropify();
       
   ////////////////////////////////////////////////////////////////////////////

   $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#member_form').parsley(); 
      // on click
      $('#addMemberBtn').click(function () {
        $('#member_form').parsley().reset();
        $('textarea,#division_logo_div').html('');
        $('#member_id').val('');
        $('#member_form').trigger("reset");
        $('#member_image_div').replaceWith('<div id="member_image_div"></div>');
        $('#old_member_image').val('');
        $('#member_folder').val('');
        //   $('.dropify').clearElement();
        $('#memberTitle').html("Add New Member");
        $('#actionType').val("create");
        $('#member_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="member_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
        $('#addMember').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editMember', function () {
        var data_id = $(this).data('id');
        var url = '{{ route("member_manage.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
        $('#memberTitle').html("Edit Member");
        $('#member_id').val(data.id);
        $('#member_first_name').val(data.member_first_name);
        $('#member_middle_name').val(data.member_middle_name);
        $('#member_last_name').val(data.member_last_name);
        $('#member_dob').val(data.member_dob);
        $('#member_tel').val(data.member_tel);
        $('#member_emergency_tel').val(data.member_emergency_tel);
        $('#member_email').val(data.member_email);
        $('#member_region').val(data.member_region);
        $('#member_image_div').replaceWith('<div id="member_image_div">'+data.memberImage+'</div>');
        $('#member_address').val(data.member_address);
        $('#member_location').val(data.member_location);
        $('#member_professional_number').val(data.member_professional_number);
        $('#member_type').val(data.member_type);
        $('#member_designation_id').val(data.member_designation_id).trigger('change');
        $('#member_company_name').val(data.member_company_name);
        $('#member_company_type').val(data.member_company_type);
        $('#member_company_tel').val(data.member_company_tel);
        $('#member_company_email').val(data.member_company_email);
        $('#member_company_region').val(data.member_company_region);
        $('#member_office_address').text(data.member_office_address);
        $('#member_office_location').text(data.member_office_location);
        $('#member_notes').text(data.member_notes);
        $('#old_member_image').val(data.member_image);
        $('#member_folder').val(data.member_folder);
        $('#actionType').val("edit");
        $('#member_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="member_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
        $('#addMember').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteMember', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("member_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_member').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#member_form").submit(function(e){
        e.preventDefault();
        $.ajax({
            data:new FormData($('#member_form')[0]),
            enctype: 'multipart/form-data', 
            url: "{{ route('member_manage.store') }}",
            contentType:false,
            cache: false,
            processData:false,
            type: "POST",
            // beforeSend:function(){  
            //     $('#member_save_btn').text("Loading ...").prop("disabled",true); 
            // },
            success: function (data) {
                // console.log(data);
               switch (data.message) {
                  case 'successful':
                  $('#addMember').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_member').dataTable();
                  oTable.fnDraw(false);
                  break;
               }
               
            },
            error: function (data) {
               var errormsg = JSON.parse(data.responseText);
            //    loop through the 
               swal('Error',errormsg.message,'error')
            }
         });
      });
       
   });

</script>