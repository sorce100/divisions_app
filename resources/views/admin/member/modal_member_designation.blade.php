<div  id="addDesignation" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="designationTitle">New Member Designation</h4>
         </div>
         <form method="POST"  class="form-horizontal" id="member_designation_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-3"><label>Enter Designation Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" id="member_designation_name" name="member_designation_name" autocomplete="off" placeholder="Enter Member Designation Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Enter Notes</label> </div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <textarea name="member_designation_notes" id="member_designation_notes" rows="5" class="form-control" placeholder="Enter Member Designation Notes &hellip;" >NONE</textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <input type="hidden" name="member_designation_id" id="member_designation_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="member_designation_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         <form>
      </div>
   </div>
</div>

<<script>
   $(document).ready(function() {
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#member_designation_form').parsley();
      // on click
      $('#addMemberTypeBtn').click(function () {
          $('#member_designation_form').parsley().reset();
          $('#member_designation_id').val('');
          $('#member_designation_form').trigger("reset");
          $('#memberdesignationTitle').html("New Member Designation");
          $('#actionType').val("create");
          $('#member_designation_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="member_designation_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
          $('#addDesignation').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editMemberType', function () {
        $('#member_designation_form').trigger("reset");
        var data_id = $(this).data('id');
        var url = '{{ route("member_designation.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
          $('#memberdesignationTitle').html("Edit Member Designation");
          $('#member_designation_id').val(data.id);
          $('#member_designation_name').val(data.member_designation_name);
          $('#member_designation_note').html(data.member_designation_note);
          $('#actionType').val("edit");
          $('#member_designation_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="member_designation_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
          $('#addDesignation').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteMemberType', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("member_designation.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_member_designation').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#member_designation_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#member_designation_form').serialize(),
          url: "{{ route('member_designation.store') }}",
          type: "POST",
          success: function (data) {
            //  console.log(data);
              switch (data.message) {
                case 'successful':
                  $('#addDesignation').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_member_designation').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });

</script>