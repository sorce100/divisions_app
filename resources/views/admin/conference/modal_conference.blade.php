<div  id="addconference" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="conferenceTitle">Add New Conference</h4>
         </div>
         <form method="POST" class="form-horizontal" id="conference_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Title</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <textarea name="conference_title" id="conference_title" rows="3" class="form-control" autocomplete="off" placeholder="Title for Conference" required></textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Host</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" id="conference_host" name="conference_host" autocomplete="off" placeholder="Enter Conference Host" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Target</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <select name="conference_target" id="conference_target" class="form-control" required>
                           <option selected disabled>Please Select</option>
                           <option value="Moderators">Moderators</option>
                           <option value="Members">Members</option>
                           <option value="Trainees">Trainees</option>
                           <option value="Guest">Guest</option>
                           <option value="All">All</option>
                        </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Start Date</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" data-toggle="datepicker" id="conference_start_date" name="conference_start_date" autocomplete="off" placeholder="Click to select start date" required readonly>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Start Date</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" data-toggle="datepicker" id="conference_end_date" name="conference_end_date" autocomplete="off" placeholder="Click to select start date" required readonly>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Room Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" id="conference_room_name" name="conference_room_name" autocomplete="off" placeholder="Conference room name" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Conference Notes</label> </div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <textarea name="conference_notes" id="conference_notes" rows="7" class="form-control" autocomplete="off" placeholder="Conference notes" ></textarea>
                     </div>
                  </div>
               </div>
               <!--  -->


               <input type="hidden" name="conference_id" id="conference_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="conference_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>
    $(document).ready(function() {
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#conference_form').parsley();
      // on click
      $('#addconferenceBtn').click(function () {
          $('#conference_form').parsley().reset();
          $('#conference_id').val('');
          $('#conference_form').trigger("reset");
          $('#conferenceTitle').html("Add New Conference");
          $('#actionType').val("create");
          $('#conference_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="conference_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
          $('#addconference').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editConference', function () {
        var data_id = $(this).data('id');
        var url = '{{ route("conference_manage.edit", ":id") }}';
        url = url.replace(':id', data_id); 
        $.get(url, function (data) { 
          $('#conferenceTitle').html("Edit Conference");
          $('#conference_id').val(data.id);
          $('#conference_title').html(data.conference_title);
          $('#conference_host').val(data.conference_host);
          $('#conference_target').val(data.conference_target).trigger('change'); 
          $('#conference_start_date').val(data.conference_start_date); 
          $('#conference_end_date').val(data.conference_end_date); 
          $('#conference_room_name').val(data.conference_room_name);
          $('#conference_notes').html(data.conference_notes);
          $('#actionType').val("edit");
          $('#conference_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="conference_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
          $('#addconference').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteConference', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("conference_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_conference').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#conference_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#conference_form').serialize(),
          url: "{{ route('conference_manage.store') }}",
          type: "POST",
          success: function (data) {
              switch (data.message) {
                case 'successful':
                  $('#addconference').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_conference').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
             console.log(data.responseText);
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });


  </script>