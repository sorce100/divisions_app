@extends('layouts.header')
@section('pageTitle','Setup Video Confernce')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Manage Conference Setup</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
            <button class="btn btn-primary" id="addconferenceBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
          <table id="table_conference" class="table display" style="width:100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Conference Host</th>
                <th>Conference Title</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Updated</th>
                <th>Action</th>
              </tr>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  @include('admin.conference.modal_conference')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_conference').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('conference_manage.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id','visible': false},
                      { data: 'conference_title'},
                      { data: 'conference_host'},
                      { data: 'conference_start_date'},
                      { data: 'conference_end_date'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

