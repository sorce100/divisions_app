@extends('layouts.header')
@section('pageTitle','Class Room Attendance')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Division Class Room Attendance</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->    
   <div class="row">
        <div class="white-box">
            <h2 class="header-title">Filter</h2>
        </div>
   </div>
    <!--  -->
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
          <div class="table-responsive">
          <table id="table_conference_attendance" class="table display" style="width:100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Member Type</th>
                <th>Member Name</th>
                <th>Class Room Name</th>
                <th>Attendance Date</th>
              </tr>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_conference_attendance').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('lecturer_attendance.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id','visible': false},
                      { data: 'member_type','visible': false},
                      { data: 'full_name'},
                      { data: 'conference_room_name'},
                      { data: 'updated_at'},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

