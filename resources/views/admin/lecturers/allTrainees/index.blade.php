@extends('layouts.header')
@section('pageTitle','Manage User Groups')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">All Trainee Records</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <!-- <h2 class="header-title">
            <button class="btn btn-primary" id="addGroupBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr> -->
          <div class="table-responsive">
          <table id="table_all_trainees" class="table display" style="width:100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Account Type</th>
                <th>Trainee Name</th>
                <th>Trainee Member Type</th>
                <th>Trainee Year</th>
                <th>Email</th>
                <th>Tel No</th>
                <th>Region</th>
              </tr>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_all_trainees').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('lecturer_all_trainees.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id','visible': false},
                      { data: 'user_type'},
                      { data: 'member_full_name'},
                      { data: 'member_full_name'},
                    //   change for year student registered
                      { data: 'id'},
                      { data: 'member_email'},
                      { data: 'member_tel'},
                      { data: 'member_region'},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

