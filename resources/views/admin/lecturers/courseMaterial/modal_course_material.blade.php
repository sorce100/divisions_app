<div  id="addCourseMat" class="modal fade" role="dialog">
   <div class="modal-dialog modal-xl">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="courseMatTitle">Add New Course Material</h4>
         </div>
         <form enctype="multipart/form-data" method="POST" class="form-horizontal" id="course_material_form">
            <div class="modal-body">
                <!-- modal contnet -->
                <!--  -->
                <div class="row">
                    <div class="col-md-3"><label>Select Course</label> <span class="asterick">*</span></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" name="course_material_course_id" id="course_material_course_id">
                                <option disabled selected>Select Course</option>
                                @foreach($courseNames as $courseName)
                                <option id="{{ $courseName->course_folder }}" value="{{ $courseName->id }}">{{ $courseName->course_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="row">
                    <div class="col-md-3"><label>Course Folder</label> <span class="asterick">*</span></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" name="course_material_course_folder" id="course_material_course_folder" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="row">
                    <div class="col-md-3"><label>Upload Title</label> <span class="asterick">*</span></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" name="course_material_upload_title" id="course_material_upload_title" placeholder='Eg. For lecture on Geodesy' class="form-control">
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="row m-b-10">
                    <div class="col-md-3"><label>Select Target </label> <span class="asterick">*</span></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select name="course_material_target" id="course_material_target" class="form-control">
                                <option selected disabled> Please Select </option>
                                <option value="Trainees">Trainees</option>
                                <option value="Members">Members</option>
                                <option value="Moderators">Moderators</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--  -->
                <hr>
                <!--  -->
                <div class="row">
                    <div class="col-md-3">
                        <label for="title" class="col-form-label">Product Images<span class="asterick"> *</span>
                        <br>
                        <span class="asterick">(Max Upload Files: 5)</span>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group m-b-15">
                            <input type="file" class="form-control course_material_files" name="course_material_files[]" >
                            <span class="input-group-btn add_new_course_material">
                                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                            </span> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span class="asterick"><b> ( PDF , Word Files , Excel ONLY )</b></span>
                    </div>
                </div>
                <!--  -->
                <div class="course_material_div"></div>
                <!-- for uploaded files -->
                <div class="old_course_material_div"></div>
                <!--  -->
                <hr>
                <!--  -->
                <div class="row">
                    <div class="col-md-3"><label>Uploads Notes</label> </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <textarea name="course_material_notes" id="course_material_notes" class="form-control" rows="5" placeholder="Any additional Notes"></textarea>
                        </div>
                    </div>
                </div>
                <!--  -->
                
                <!--  -->
                <input type="hidden" name="uploaded_course_material_files_array" id="uploaded_course_material_files_array" value="">
                <input type="hidden" name="course_material_id" id="course_material_id" value="">
                <input type="hidden" name="actionType" id="actionType" value="create">
                <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                <!--end modal contnet -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-primary" id="course_material_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
        </form>
      </div>
   </div>
</div>

<script>
   $(document).ready(function(){
        let max_fields      = 5; //maximum input boxes allowed
        let wrapper         = $(".course_material_div"); //Fields wrapper
        let add_button      = $(".add_new_course_material"); //Add button ID

        let x = 1; //initlal text box count

        $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            //////////////////////////////////////////////////////////////
            $(wrapper).append(
                                '<div class="row">'+ 
                                    '<div class="col-md-3"></div>'+ 
                                    '<div class="col-md-6">'+ 
                                        '<div class="input-group m-b-15">'+
                                            '<input type="file" class="form-control" name="course_material_files[]">'+
                                            '<span class="input-group-btn">'+
                                                '<button type="button" class="btn btn-danger remove_field"><i class="fa fa-trash"></i></button>'+
                                            '</span>'+ 
                                        '</div>'+ 
                                    '</div>'+ 
                                    '<div class="col-md-3"></div>'+ 
                                '</div>'
                            );
        }
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).closest('.row').remove(); x--;;
        });
    });
       
   ////////////////////////////////////////////////////////////////////////////
   $('#course_material_course_id').on('change', function() {
        var courseFolder = $('option:selected', this).prop('id');
        $('#course_material_course_folder').val(courseFolder)
    });
   ////////////////////////////////////////////////////////////////////////////
    
   $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // parsley declaration
    $('#course_material_form').parsley(); 
    // on click
    $('#addCourseMatBtn').click(function () {
        $('#course_material_form').parsley().reset();
        $('textarea,.course_material_div,.old_course_material_div').html('');
        $('#course_material_id').val('');
        $('#course_material_form').trigger("reset");
        $('#courseMatTitle').html("Add New Course Material");
        $('#actionType').val("create");
        $('#course_material_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="course_material_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
        $('#addCourseMat').modal('show');
    });

    ///////////////////////////////////// for edit modal/////////////////////////////////
    $(document).on('click', '.editCourseMat', function () {
        $('#course_material_form').trigger("reset");
        $('textarea,.course_material_div,.old_course_material_div').html('');
        var data_id = $(this).data('id');
        var url = '{{ route("lecturer_course_material.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
            $('#courseMatTitle').html("Edit Course Material");
            $('#course_material_id').val(data.id);
            $('#course_material_course_id').val(data.course_material_course_id).trigger('change');
            $('#course_material_upload_title').val(data.course_material_upload_title);
            $('#course_material_target').val(data.course_material_target).trigger('change');
            $('#course_material_notes').html(data.course_material_notes);
            // for displaying files uploaded
            var fileNamesArray = JSON.parse(data.course_material_files);
            fileNamesArray.forEach(function(fileName) {
                $('.old_course_material_div').append(
                    '<div class="row" style="background-color:#D4EBF2">'+ 
                        '<div class="col-md-3"><label for="title" class="col-form-label">Uploaded File </label></div>'+ 
                        '<div class="col-md-6">'+ 
                            '<div class="input-group m-b-15">'+
                                '<input type="text" class="form-control" name="old_course_material_files[]" value="'+fileName+'" readonly>'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-danger remove_old_field"><i class="fa fa-trash"></i></button>'+
                                '</span>'+ 
                            '</div>'+ 
                        '</div>'+ 
                        '<div class="col-md-3"></div>'+ 
                    '</div>');
            });
            $('#uploaded_course_material_files_array').val(data.course_material_files);
            $('#actionType').val("edit");
            $('#course_material_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="course_material_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
            $('#addCourseMat').modal('show');
        })
    });

    ////////////////////// onclick to remove uploaded file///////////////////////////////////
    $('.old_course_material_div').on("click",".remove_old_field", function(e){ //user click on remove text
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            e.preventDefault(); $(this).closest('.row').remove(); x--;;
            swal("Save Changes to Complete update", {
            icon: "success",
            });
        } else {
            // toastr.error('There was an error');
        }
        });
    });

    //////////////////////////// for displaying uploaded files////////////////////////////////////////
    $(document).on('click', '.listUploadedFiles', function(e){
        e.preventDefault();
        var data_id = $(this).prop('id');
        var url = '{{ route("lecturer_course_material.show", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
            $('.files_list_div').html(data);
            $('#filesListModal').modal('show');
        });
    });
    //////////////////////////// for Downloading files ////////////////////////////////////////
    $(document).on('click', '.downloadFile', function(e){
        e.preventDefault();
        var filePath = $(this).prop('id');

        $.ajax({
            data: {filePath:filePath},
            url: "{{ route('files_download.store') }}",
            type: "POST",
            success: function (data) {
                console.log(data);
                
            },
            error: function (data) {
                toastr.error('Sorry! error');
           
            }
        });
    });
    // 
    
    //////////////////////////////////// for delete////////////////////////////////////////////////////
    $(document).on('click', '.deleteCourseMat', function(e){
    e.preventDefault();
    // alert('hello');
    var data_id = $(this).data('id');
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            var data = {
                "_token": $('input[name=_token]').val(),
                "id": data_id,
            };
            var url = '{{ route("lecturer_course_material.destroy", ":id") }}';
            url = url.replace(':id', data_id);
            $.ajax({
                type:"delete",
                url:url,
                data:data,
                success: function(response){
                    // console.log(response);
                    swal(response.message, {
                    icon: "success",
                    })
                    .then((willDelete) => {
                        var oTable = $('#table_course_material').dataTable();
                        oTable.fnDraw(false);
                    }); 
                },
            });
            
        } 
    });
    });
    
      ////////////////////// for inserting and updating/////////////////////////
      $("#course_material_form").submit(function(e){
        e.preventDefault();
        $.ajax({
            data:new FormData($('#course_material_form')[0]),
            enctype: 'multipart/form-data', 
            url: "{{ route('lecturer_course_material.store') }}",
            contentType:false,
            cache: false,
            processData:false,
            type: "POST",
            beforeSend:function(){  
            $('#course_material_save_btn').text("Loading ...").prop("disabled",true); 
            },
            success: function (data) {
                // console.log(data);
               switch (data.message) {
                  case 'successful':
                  $('#addCourseMat').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_course_material').dataTable();
                  oTable.fnDraw(false);
                  break;
               }
               
            },
            error: function (data) {
            //    console.log(data);
               toastr.error('Sorry! error');
               var errormsg = JSON.parse(data.responseText);
               // loop through the 
               swal('Error',errormsg.message,'error')
            }
         });
      });
       
   });

</script>