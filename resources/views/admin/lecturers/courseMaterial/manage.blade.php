@extends('layouts.header')
@section('pageTitle','Manage Course Materials')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Manage Course Materials</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
            <button class="btn btn-primary" id="addCourseMatBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
          <table id="table_course_material" class="table display" style="width:100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Course Alias</th>
                <th>Target</th>
                <th>Total Files</th>
                <th>Uploader</th>
                <th>Upload Date</th>
                <th>Action</th>
              </tr>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  @include('admin.lecturers.courseMaterial.modal_course_material')
  @include('files_access.files_list')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_course_material').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('lecturer_course_material.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id','visible': false},
                      { data: 'course_alias'},
                      { data: 'course_material_target'},
                      { data: 'files_total'},
                      { data: 'full_name'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

