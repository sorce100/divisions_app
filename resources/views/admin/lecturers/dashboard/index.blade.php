@extends('layouts.header')
@section('pageTitle','Trainee Dashboard')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Dashboard</h4>
      <div class="clearfix"></div>
   </div>
   <!-- for lecturers assigned course -->
   <div class="page-title-box">
        <h4 class="text-center">Course Assigned : <span class="color">Geodesy</span></h4>
        <div class="clearfix"></div>
   </div>

      <!--Start row-->
    <div class="white-box ">
     <div class="row container">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <!-- content goes in here -->
            <a href="{{ route('lecturer_all_trainees.index') }}" class="btn btn-app">
              <span class="badge bg-red">{{$traineeCount}}</span>
              <i class="fa fa-users"></i> All Trainees
            </a>
            <a href="{{ route('conference_dashboard.index') }}" class="btn btn-app">
              <i class="fa fa-television"></i> Class Rooms
            </a>
            <a href="{{ route('lecturer_attendance.index') }}" class="btn btn-app">
              <i class="fa fa-graduation-cap"></i> Attendance
            </a>
            <a href="{{ route('lecturer_course_material.index') }}" class="btn btn-app">
              <i class="fa fa-upload"></i> Upload Resources
            </a>
            <a href="{{ route('lecturer_exams_result.index') }}" class="btn btn-app">
              <i class="fa fa-keyboard-o"></i> Enter Results
            </a>
            <!-- end of body content -->
          </div>
        </div>
      </div>
    </div>
  </div>
   <!--End row-->

   <!-- for students calender -->
   <div class="row">
  <div class="calendar-layout clearfix">      
    <!-- End To Do List-->   
    <div class="col-md-12">
      <div class="white-box">
        <h2 class="header-title">Trainees Calendar</h2>
        <hr>
        <div id='calendar'></div>
      </div>
    </div>
  </div>      
</div>
   <!-- end of student calendar -->
<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
