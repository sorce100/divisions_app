@extends('layouts.header')
@section('pageTitle','Manage Users')
@section('pageStyle')
<!-- for dropify -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}" />
  <!-- for toggle  -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css') }}" />
@endsection
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Manage Users</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
          <button class="btn btn-primary" id="adduserBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
            <table id="table_user" class="table display" style="width:100%">
            <thead>
              <th>ID</th>
              <th>Member Division</th>
              <th>Member Name</th>
              <th>Member Email</th>
              <th>Member Group</th>
              <th>Member Login Status</th>
              <th>Member Account Status</th>
              <th>Updated</th>
              <th>Action</th>
            </thead>
            <tbody>
              
            </tbody>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  <script type="text/javascript" src="{{ asset('assets/plugins/dropify/js/dropify.js') }}" ></script>
  <!-- for toggle  -->
  <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-toggle/bootstrap2-toggle.min.js') }}" ></script>
  <!-- modal include -->
  @include('admin.users.modal_users')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_user').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('users_manage.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id','visible': false},
                      { data: 'user_member_id'},
                      { data: 'user_division_id'},
                      { data: 'email'},
                      { data: 'user_group_id'},
                      { data: 'user_login_status_log'},
                      { data: 'user_account_status_log'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

<!-- modal for adding new task -->
