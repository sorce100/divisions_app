<div  id="addUser" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="userTitle">Add New User</h4>
         </div>
         <form method="POST" class="form-horizontal" id="user_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-2"><label>Select Division</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                     <select name="user_division_id" id="user_division_id" class="form-control">
                           <option selected disabled >Please Select</option>
                           @foreach($divisions as $division)
                              <option value="{{$division->id}}">{{$division->division_name}}</option>
                           @endforeach
                     </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Account Type</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                     <select name="user_type" id="user_type" class="form-control">
                           <option selected disabled >Please Select</option>
                           <option value="Member">Member</option>
                           <option value="Trainee">Trainee</option>
                           <option value="Admin">Administrator</option>
                     </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Select Member</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                     <select name="user_member_id" id="user_member_id" class="form-control" style="width: 100%;">
                           <option selected disabled >Please Select</option>
                           @foreach($members as $member)
                              <option id="{{$member->member_email}}" value="{{$member->id}}">{{$member->member_first_name }} {{$member->member_middle_name }} {{$member->member_last_name }}</option>
                           @endforeach
                     </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>User Email</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="text" class="form-control" id="user_email" name="email" autocomplete="off" placeholder="Select Member &hellip;" required readonly>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>User Password</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="password" class="form-control" id="user_password" name="password" autocomplete="off" placeholder="Enter Password &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Select Group</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                     <select name="user_group_id" id="user_group_id" class="form-control">
                           <option selected disabled >Please Select</option>
                           @foreach($groups as $group)
                              <option value="{{$group->id}}">{{$group->group_name}}</option>
                           @endforeach
                     </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Login Status</label> <span class="asterick">*</span></div>
                  <div class="col-md-3">
                     <div class="form-group">
                     <input type="checkbox" id="user_login_status" data-width="200" checked/>
                     <input type="hidden" name="user_login_status_log" id="user_login_status_log" value="LOGIN"/>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Account Status</label> <span class="asterick">*</span></div>
                  <div class="col-md-3">
                     <div class="form-group">
                     <input type="checkbox" id="user_account_status" data-width="200"checked/>
                     <input type="hidden" name="user_account_status_log" id="user_account_status_log" value="ACTIVE"/>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Notes</label> </div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <textarea name="user_notes" id="user_notes" rows="8" class="form-control" placeholder="Enter User Account Notes &hellip;" required></textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <input type="hidden" name="user_id" id="user_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="user_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
         </div>
   </div>
</div>

<script>
   $(document).ready(function(){
      // for select 2
      $("#user_member_id").select2({
	      dropdownParent: $("#addUser")
	    });
      // declare dropify
      $('.dropify').dropify();
      // for user login status
      $(function() {
        $('#user_login_status').bootstrapToggle({
        on: 'DIRECT LOGIN',
        off: 'RESET PASSWORD',
        onstyle: 'success',
        offstyle: 'danger'
        });
        $('#user_login_status').change(function(){
        if($(this).prop('checked')){
            $('#user_login_status_log').val('LOGIN');
        }else{
            $('#user_login_status_log').val('RESET_PASSWORD');
        }
        });
      });
    //   for account active status
      $(function() {
        $('#user_account_status').bootstrapToggle({
        on: 'ACTIVE ACCOUNT',
        off: 'INACTIVE ACCOUNT',
        onstyle: 'success',
        offstyle: 'danger'
        });
        $('#user_account_status').change(function(){
        if($(this).prop('checked')){
            $('#user_account_status_log').val('ACTIVE');
        }else{
            $('#user_account_status_log').val('INACTIVE');
        }
        });
      });
      ///////////////////////////////////////////////////////////////
      // get member email and insert

      $('#user_member_id').on('change', function() {
         var memberemail = $('option:selected', this).prop('id');
         $('#user_email').val(memberemail)
      });


      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#user_form').parsley();
      // on click
      $('#adduserBtn').click(function () {
          $('#user_form').parsley().reset();
          $('#user_id').val('');
          $('#user_form').trigger("reset");
          $('#userTitle').html("Add New User");
          $('#actionType').val("create");
          $('#user_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="user_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
          $('#addUser').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editUser', function () {
        var data_id = $(this).data('id');
        var url = '{{ route("users_manage.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
         //   console.log(data.group_module_id.length);
          $('#userTitle').html("Edit User");
          $('#user_id').val(data.id);
          $('#user_division_id').val(data.user_division_id).trigger('change');
          $('#user_type').val(data.user_type).trigger('change');
          $('#user_member_id').val(data.user_member_id).trigger('change');
          $('#user_email').val(data.email);
          $('#user_group_id').val(data.user_group_id).trigger('change'); 
         //  
          switch($.trim(data.user_login_status_log)) {
            case 'LOGIN':
               $('#user_login_status').bootstrapToggle('on');
            break;
            case 'RESET_PASSWORD':
               $('#user_login_status').bootstrapToggle('off');
            break;
         }
         // 
         switch($.trim(data.user_account_status_log)) {
            case 'ACTIVE':
               $('#user_account_status').bootstrapToggle('on');
            break;
            case 'INACTIVE':
               $('#user_account_status').bootstrapToggle('off');
            break;
         }
         // 
         $('#user_notes').html(data.user_notes);
          $('#actionType').val("edit");
          $('#user_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="user_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
          $('#addUser').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteUser', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("users_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_user').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#user_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#user_form').serialize(),
          url: "{{ route('users_manage.store') }}",
          type: "POST",
          success: function (data) {
              switch (data.message) {
                case 'successful':
                  $('#addUser').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_user').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
             console.log(data.responseText);
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });


  </script>