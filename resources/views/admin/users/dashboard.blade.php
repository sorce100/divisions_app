@extends('layouts.header')
@section('pageTitle','Users Dashboard')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Users Dashboard</h4>
      <div class="clearfix"></div>
   </div>
      <!--Start row-->
    <div class="white-box ">
     <div class="row container">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <!-- content goes in here -->
            <a href="#" class="btn btn-app">
              <span class="badge bg-red">New</span>
              <i class="fa fa-briefcase"></i>New Request
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-plus"></i> Create New Users
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-users"></i> Total Users
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-laptop"></i>Users Online
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-power-off"></i> Users Offline
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-ban"></i> Blocked Users
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-bell"></i> Accounts Issues
            </a>
            <!-- end of body content -->
          </div>
        </div>
      </div>
    </div>
  </div>
   <!--End row-->

   <!--  -->
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">Recent Account Activities</h2>
          <hr>
          <div class="table-responsive">
           <table id="" class="tableInit table">
            <thead>
              <th>Division</th>
              <th>Account Name</th>
              <th>Group</th>
              <th>Date Updated</th>
              <th>Updated By</th>
            </thead>
            <tbody>
              <tr>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
              </tr>
              <tr>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
              </tr>
              <tr>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
              </tr>
              <tr>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
                <td>Sorce Kwarteng</td>
              </tr>
            </tbody>
           </table>  
          </div>
         </div>
     </div>
   </div>
   <!--  -->


<!-- end  -->
</div>
@endSection

