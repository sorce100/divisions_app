@extends('layouts.header')
@section('pageTitle','Manage Modules')
@section('pageBody')

<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Manage Modules</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
            <button class="btn btn-primary" id="addModuleBtn" ><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
            <table id="table_module" class="table display" style="width:100%">
              <thead class="thead_bg">
                <tr>
                  <th>ID</th>
                  <th>Division</th>
                  <th>Module Name</th>
                  <th>Module Category</th>
                  <th>Last Updated</th>
                  <th>Action</th>
                </tr>
              </thead>
              
            </table>
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
<!-- modal included -->
  @include('admin.module.modal_module')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_module').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('module_manage.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id', name: 'id', 'visible': false},
                      { data: 'division_alias'},
                      { data: 'module_name'},
                      { data: 'module_category'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection
