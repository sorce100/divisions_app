<div  id="addModule" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="moduleTitle">Add New Module</h4>
         </div>
         
         <form method="POST"  class="form-horizontal" id="module_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Select Division</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <select name="module_division_id" id="module_division_id" class="form-control" required>
                           <option selected disabled>Please Select</option>
                           @foreach($divisions as $division)
                           <option value="{{$division->id}}">{{$division->division_alias}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Module Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <input type="text" class="form-control" id="module_name" name="module_name" autocomplete="off" placeholder="Enter Module Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Module Category</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <select name="module_category" id="module_category" class="form-control" required>
                           <option selected disabled>Please Select</option>
                           <option value="Administrator">Administrator</option>
                           <option value="Member">Member</option>
                           <option value="Trainee">Trainee</option>
                        </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-3"><label>Module URL</label> <span class="asterick">*</span></div>
                  <div class="col-md-9">
                     <div class="form-group">
                     <textarea name="module_url" id="module_url" rows="20" class="form-control" placeholder="Enter Module URL &hellip;" required> </textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <input type="hidden" name="module_id" id="module_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="module_save_btn" >Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>


<script>
    $(document).ready(function() {
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#module_form').parsley();
      // on click
      $('#addModuleBtn').click(function () {
          $('#module_form').parsley().reset();
          $('#module_id').val('');
          $('#module_url').html('');
          $('#module_form').trigger("reset");
          $('#moduleTitle').html("Add New Module");
          $('#actionType').val("create");
          $('#module_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="module_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
          $('#addModule').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editModule', function () {
        $('#module_form').trigger("reset");
        var data_id = $(this).data('id');
        var url = '{{ route("module_manage.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
          $('#moduleTitle').html("Edit Module");
          $('#module_id').val(data.id);
          $('#module_name').val(data.module_name);
          $('#module_category').val(data.module_category).trigger('change'); 
          $('#module_division_id').val(data.module_division_id).trigger('change');
          $('#module_url').html(data.module_url);
          $('#actionType').val("edit");
          $('#module_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="module_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
          $('#addModule').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteModule', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("module_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_module').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#module_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#module_form').serialize(),
          url: "{{ route('module_manage.store') }}",
          type: "POST",
          success: function (data) {
              switch (data.message) {
                case 'successful':
                  $('#addModule').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_module').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });


  </script>