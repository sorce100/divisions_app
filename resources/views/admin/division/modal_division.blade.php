<div  id="addDivision" class="modal fade" role="dialog">
   <div class="modal-dialog modal-xl">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="divisionTitle">Add New Division</h4>
         </div>
         <form enctype="multipart/form-data" method="POST" class="form-horizontal" id="division_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-2"><label>Division Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="text" class="form-control" id="division_name" name="division_name" autocomplete="off" placeholder="Enter Division Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Division Alias</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="text" class="form-control" id="division_alias" name="division_alias" autocomplete="off" placeholder="Enter Divison Alias Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Contact Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-5">
                     <div class="form-group">
                        <input type="text" class="form-control" id="division_contact_fname" name="division_contact_fname" autocomplete="off" placeholder="Enter Divison Contact First Name &hellip;" required>
                     </div>
                  </div>
                  <div class="col-md-5">
                     <div class="form-group">
                        <input type="text" class="form-control" id="division_contact_lname" name="division_contact_lname" autocomplete="off" placeholder="Enter Divison Contact First Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Contact Tel</label> <span class="asterick">*</span></div>
                     <div class="col-md-10">
                        <div class="form-group">
                           <input type="number" class="form-control" id="division_contact_tel" name="division_contact_tel" autocomplete="off" >
                        </div>
                     </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Division Address</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <textarea name="division_address" id="division_address" rows="6" class="form-control" placeholder="Enter Divison Address &hellip;" required></textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Division Location</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <textarea name="division_location" id="division_location" rows="6" class="form-control" placeholder="Enter Location Of Division &hellip;" required></textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Division Status</label> <span class="asterick">*</span></div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="checkbox" id="division_status" data-width="200" checked/>
                        <input type="hidden" name="division_status_log" id="division_status_log" value="ACTIVE" />
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Division Logo</label> <span class="asterick">*</span></div>
                  <div class="col-md-5">
                     <div class="form-group">
                        <input type="file" name="division_logo" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="jpg png jpeg">
                     </div>
                  </div>
                  <div class="col-md-5">
                     <div id="division_logo_div"></div>
                  </div>
               </div>
               <!--  -->
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Division Notes</label> </div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <textarea name="division_notes" id="division_notes" rows="12" class="form-control" placeholder="Enter Division Notes &hellip;" required></textarea>
                     </div>
                  </div>
               </div>
               <!--  -->
               <input type="hidden" name="old_division_logo" id="old_division_logo" value="">
               <input type="hidden" name="division_folder" id="division_folder" value="">
               <input type="hidden" name="division_id" id="division_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!-- progress bar -->
               <div class="row">
                  <div class="col-md-12">
                     <!--for progress modal  -->
                     <div class="progress">
                     <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;height:2%;"> 0% </div>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="division_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>
   $(document).ready(function(){
      // declare dropify
      $('.dropify').dropify();
      // for division toggle
      $(function() {
         $('#division_status').bootstrapToggle({
            on: 'DIVISION ACTIVE',
            off: 'DIVISION INACTIVE',
            onstyle: 'success',
            offstyle: 'danger'
         });
         $('#division_status').change(function(){
            if($(this).prop('checked')){
               $('#division_status_log').val('ACTIVE');
            }else{
               $('#division_status_log').val('INACTIVE');
            }
         });
      });
      ////////////////////////////////////////////////////////////////////////////

      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#division_form').parsley();
      // on click
      $('#addDivisionBtn').click(function () {
         $('#division_status').bootstrapToggle('on');
         $('#division_form').parsley().reset();
         $('textarea,#division_logo_div').html('');
         $('#division_id').val('');
         $('#division_form').trigger("reset");
         $('#divisionTitle').html("Add New Division");
         $('#actionType').val("create");
         $('#division_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="division_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
         $('#addDivision').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editDivision', function () {
        var data_id = $(this).data('id');
        var url = '{{ route("division_manage.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
         $('#divisionTitle').html("Edit Division");
         $('#division_id').val(data.id);
         $('#division_name').val(data.division_name);
         $('#division_alias').val(data.division_alias);
         $('#division_contact_fname').val(data.division_contact_fname);
         $('#division_contact_lname').val(data.division_contact_lname);
         $('#division_contact_tel').val(data.division_contact_tel);
         $('#division_address').html(data.division_address);
         $('#division_location').html(data.division_location);
         // $('#division_status_log').val(data.module_name); 
         switch($.trim(data.division_status_log)) {
            case 'INACTIVE':
            $('#division_status').bootstrapToggle('off');
            break;
            case 'ACTIVE':
            $('#division_status').bootstrapToggle('on');
            break;
         }
         $('#division_logo_div').replaceWith('<div id="division_logo_div">'+data.logoImage+'</div>');
         $('#division_notes').html(data.division_notes);
         $('#old_division_logo').val(data.division_logo);
         $('#division_folder').val(data.division_folder);
         $('#actionType').val("edit");
         $('#division_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="division_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
         $('#addDivision').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteDivision', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                  "_token": $('input[name=_token]').val(),
                  "id": data_id,
                };
                var url = '{{ route("division_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_division').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#division_form").submit(function(e){
        e.preventDefault();
        $.ajax({
            // for progress bar
            xhr:function(){
               let xhr = new XMLHttpRequest();
               xhr.upload.addEventListener('progress',function(e){
                  // check if upload length is true or false
                  if (e.lengthComputable) {
                  let uploadPercent = Math.round((e.loaded/e.total)*100);
                  // updating progress bar pecentage
                  $('#progressBar').prop('aria-valuemax',uploadPercent).css('width',uploadPercent + '%').text(uploadPercent + '%');
                  }
               });
               return xhr;
            },
            data:new FormData($('#division_form')[0]),
            enctype: 'multipart/form-data', 
            url: "{{ route('division_manage.store') }}",
            contentType:false,
            cache: false,
            processData:false,
            type: "POST",
            beforeSend:function(){  
            $('#division_save_btn').text("Loading ...").prop("disabled",true); 
            },
            success: function (data) {
               switch (data.message) {
                  case 'successful':
                  $('#addDivision').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_division').dataTable();
                  oTable.fnDraw(false);
                  break;
               }
               
            },
            error: function (data) {
               // console.log(data);
               toastr.error('Sorry! error');
               var errormsg = JSON.parse(data.responseText);
               // loop through the 
               swal('Error',errormsg.message,'error')
            }
         });
      });
       
   });

</script>