@extends('layouts.header')
@section('pageTitle','Division Dashboard')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Division Dashboard</h4>
      <div class="clearfix"></div>
   </div>
      <!--Start row-->
    <div class="white-box ">
     <div class="row container">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <!-- content goes in here -->
            <a href="trainee_results.php" class="btn btn-app">
              <span class="badge bg-red">10</span>
              <i class="fa fa-archive"></i>All Division
            </a>
            <a href="#" class="btn btn-app">
              <span class="badge bg-red">500</span>
              <i class="fa fa-users"></i>Total Members
            </a>
            <a href="#" class="btn btn-app">
              <span class="badge bg-red">5</span>
              <i class="fa fa-lock"></i> All Accounts
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-cog"></i> empty
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-cog"></i> empty
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-cog"></i> empty
            </a>
            <a href="#" class="btn btn-app">
              <i class="fa fa-cog"></i> empty
            </a>
            <!-- end of body content -->
          </div>
        </div>
      </div>
    </div>
  </div>
   <!--End row-->


<!-- end  -->
</div>
@endSection

