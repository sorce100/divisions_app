@extends('layouts.header')
@section('pageTitle','Manage SMS')
@section('pageBody')

<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Manage Sms</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
            <button class="btn btn-primary" id="addSmsBtn" ><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
            <table id="table_sms" class="table display" style="width:100%">
              <thead class="thead_bg">
                <tr>
                  <th width="10%">Receiver Type</th>
                  <th width="10%">Receiver Name</th>
                  <th width="10%">Receiver Tel</th>
                  <th width="10%">Reference Type</th>
                  <th width="34%">Message Content</th>
                  <th width="10%">Response</th>
                  <th width="10%">Last Updated</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              
            </table>
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
<!-- modal included -->
  @include('admin.sms.modal_sms')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_sms').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('sms_manage.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'member_type_name'},
                      { data: 'member_full_name'},
                      { data: 'sms_destination_tel'},
                      { data: 'sms_reference_type'},
                      { data: 'sms_send_message'},
                      { data: 'sms_response'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection
