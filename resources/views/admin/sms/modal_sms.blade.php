<div  id="addsms" class="modal fade" role="dialog">
   <div class="modal-dialog modal-xl">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="smsTitle">Add New Sms</h4>
         </div>
         <form method="POST" class="form-horizontal" id="sms_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-7" >
                     <!--  -->
                     <div class="row">
                        <div class="col-md-3"><label>SMS Reference</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <select name="sms_reference_type" id="sms_reference_type" class="form-control" required>
                                 <!-- <option selected disabled>Please Select</option>
                                 <option value="Single">Single Sms</option> -->
                                 <option value="Bulk">Bulk Sms</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <!--  -->
                     <div class="row">
                        <div class="col-md-3"><label>Member Type</label> <span class="asterick">*</span></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <select id="sms_reciever_member_type" class="form-control" required>
                                 <option selected disabled>Please Select</option>
                                 <option value="0">All</option>
                                 @foreach($memberTypes as $type)
                                 <option value="{{$type->id}}">{{$type->member_type_name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                     <!--  -->
                     <hr>
                     <div class="row">
                        <div class="col-md-12">
                           <label>Select Member Receipient</label> <span class="asterick">*</span>
                           <br>
                           <div class="table-responsive">
                              <table class="table table-bordered">
                                 <thead>
                                    <th><input type="checkbox" id="checkAll" ></th>
                                    <th>Member Name</th>
                                    <th>Member Type</th>
                                    <th>Tel Number</th>
                                 </thead>
                                 <tbody id="sms_reciever_member_id_div"></tbody>
                              </table>
                           </div>
                        </div>
   
                     </div>
                  </div>
                  <div class="col-md-5">
                     <!-- for sms counter -->
                     <p><span id="remaining">160 characters remaining</span> <span id="messages" style="color:red;">1 message(s)</span></p>
                     <!--  -->
                     <div class="row m-b-10">
                        <div class="col-md-12">
                           <div class="form-group">
                              <textarea name="sms_send_message" id="sms_send_message" rows="8" class="form-control" required placeholder="Enter Sms Message"> </textarea>
                           </div>
                        </div>
                     </div>
                     
                     <!--  -->
                  </div>
               </div>

               <input type="hidden" name="sms_id" id="sms_id" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="submit" class="btn btn-primary" id="sms_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>
    $(document).ready(function() {
      //  for sms counter
      var $remaining = $('#remaining'),
      $messages = $remaining.next();
      $('#sms_send_message').keyup(function(){
         var chars = this.value.length,
         messages = Math.ceil(chars / 160),
         remaining = messages * 160 - (chars % (messages * 160) || messages * 160);
         $remaining.text(remaining + ' characters remaining');
         $messages.text(messages + ' message(s)');
      });
      // end of sms counter
      //////////////////////////////// 
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#sms_form').parsley();

      // for selecting members receivers
      $('#sms_reciever_member_type').on('change', function() {
         var data_id = $('option:selected', this).val();
         var url = '{{ route("sms_manage.show", ":id") }}';
         url = url.replace(':id', data_id);
         $.get(url, function (data) {
            // console.log(data);
            $('#sms_reciever_member_id_div').html(data);
         
        })
      });


      // on click
      $('#addSmsBtn').click(function () {
          $('#sms_form').parsley().reset();
          $('#sms_id').val('');
          $('#sms_form').trigger("reset");
          $('#sms_reciever_member_id_div').html('');
          $('#smsTitle').html("Add New Sms");
          $('#actionType').val("create");
          $('#sms_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="sms_save_btn">Send SMS <i class="fa fa-send"></i></button>');
          $('#addsms').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editGroup', function () {
        var data_id = $(this).data('id');
        var url = '{{ route("sms_manage.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
         //   console.log(data.group_module_id.length);
         $('#smsTitle').html("Edit Group");
         $('#sms_id').val(data.id);
         $('#group_division_id').val(data.group_division_id).trigger('change');
         $('#group_name').val(data.group_name);
         $('.group_module_id').val(data.group_module_id).trigger('change'); 
         $('#actionType').val("edit");
         $('#sms_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="sms_save_btn">Send SMS <i class="fa fa-send"></i></button>');
         $('#addsms').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteGroup', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("sms_manage.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_sms').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#sms_form").submit(function(e){
        e.preventDefault();

        $.ajax({
         data: $('#sms_form').serialize(),
         url: "{{ route('sms_manage.store') }}",
         type: "POST",
         beforeSend:function(){  
            $('#sms_save_btn').text("Loading ...").prop("disabled",true); 
         },
          success: function (data) {
            //  console.log(data);
              switch (data.message) {
                case 'successful':
                  $('#addsms').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_sms').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
            //  console.log(data.responseText);
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });


  </script>