@extends('layouts.header')
@section('pageTitle','Setup Courses')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Setup Courses</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">
            <button class="btn btn-primary" id="addCourseBtn"><i class="fa fa-plus"></i> Add New </button>
         </h2>
          <hr>
          <div class="table-responsive">
          <table id="table_course" class="table display" style="width:100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Course Name</th>
                <th>Course Alias</th>
                <th>Course Year</th>
                <th>Course Year Part</th>
                <th>Course Lecturer</th>
                <th>Updated</th>
                <th>Action</th>
              </tr>
            </thead>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endsection

@section('pageScript')
  @include('admin.traineemoderator.setupCourse.modal_setup_course')
  <script>
    $(document).ready(function() {
      // inital datatable live
      $('#table_course').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "{{ route('moderator_setup_course.index') }}",
              type: 'GET',
            },
            columns: [
                      { data: 'id','visible': false},
                      { data: 'course_name'},
                      { data: 'course_alias'},
                      { data: 'course_year'},
                      { data: 'course_year_part'},
                      { data: 'full_name'},
                      { data: 'updated_at'},
                      { data: 'action', orderable: false},
                  ],
            order: [[0, 'desc']]
            
      });
    });
  </script>
@endsection

