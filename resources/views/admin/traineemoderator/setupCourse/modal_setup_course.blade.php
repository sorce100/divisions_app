<div  id="addCourse" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="courseTitle">Add New Course</h4>
         </div>
         <form method="POST" class="form-horizontal" id="course_form">
            <div class="modal-body">
               <!-- modal contnet -->
               <div class="row">
                  <div class="col-md-2"><label>Course Name</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="text" class="form-control" id="course_name" name="course_name" autocomplete="off" placeholder="Enter Course Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Course Alias</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <input type="text" class="form-control" id="course_alias" name="course_alias" autocomplete="off" placeholder="Enter Group Name &hellip;" required>
                     </div>
                  </div>
               </div>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Course Year</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <select name="course_year" id="course_year" class="form-control" required>
                           <option disabled selected>Please Select</option>
                           <option value="Year 1">Year 1</option>
                           <option value="Year 2">Year 2</option>
                           <option value="Year 3">Year 3</option>
                           <option value="Year 4">Year 4</option>
                           <option value="Year 5">Year 5</option>
                        </select>
                     </div>
                  </div>
               </div>
                <!--  -->
                <div class="row">
                  <div class="col-md-2"><label>Course Part</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <select name="course_year_part" id="course_year_part" class="form-control" required>
                           <option disabled selected>Please Select</option>
                           <option value="Part 1">Part 1</option>
                           <option value="Part 2">Part 2</option>
                           <option value="Part 3">Part 3</option>
                           <option value="Part 4">Part 4</option>
                           <option value="Part 5">Part 5</option>
                        </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
                <!--  -->
                <div class="row">
                  <div class="col-md-2"><label>Assign Lecturer</label> <span class="asterick">*</span></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <select name="course_lecturer_member_id" id="course_lecturer_member_id" class="form-control" style="width: 100%;">
                           <option disabled selected>Please Select</option>
                            @foreach($members as $row)
                                <option value="{{ $row->id }}">{{ $row->member_first_name.' '.$row->member_middle_name.' '.$row->member_last_name }}</option>
                            @endforeach
                        </select>
                     </div>
                  </div>
               </div>
               <!--  -->
               <hr>
               <!--  -->
               <div class="row">
                  <div class="col-md-2"><label>Course Notes</label></div>
                  <div class="col-md-10">
                     <div class="form-group">
                        <textarea name="course_notes" id="course_notes" rows="6" class="form-control" placeholder="Enter Course Notes &hellip;"></textarea>
                     </div>
                  </div>
               </div>
                <!--  -->
    
               <!--  -->
               <input type="hidden" name="course_id" id="course_id" value="">
               <input type="hidden" name="course_folder" id="course_folder" value="">
               <input type="hidden" name="actionType" id="actionType" value="create">
               <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
               <button type="button" class="btn btn-primary" id="course_save_btn">Save <i class="fa fa-save"></i></button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>
    $(document).ready(function() {
       // for select 2
      $("#course_lecturer_member_id").select2({
	      dropdownParent: $("#addCourse")
       });
      //  /
      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      // parsley declaration
      $('#course_form').parsley();
      // on click
      $('#addCourseBtn').click(function () {
          $('#course_form').parsley().reset();
          $('#course_id').val('');
          $('#course_form').trigger("reset");
          $('#courseTitle').html("Add New Course");
          $('#actionType').val("create");
          $('#course_save_btn').replaceWith('<button type="submit" class="btn btn-primary" id="course_save_btn" value="create">Save <i class="fa fa-save"></i></button>');
          $('#addCourse').modal('show');
      });

      // for edit modal
      $('body').on('click', '.editCourse', function () {
        var data_id = $(this).data('id');
        var url = '{{ route("moderator_setup_course.edit", ":id") }}';
        url = url.replace(':id', data_id);
        $.get(url, function (data) {
         //   console.log(data.group_module_id.length);
          $('#courseTitle').html("Edit Course");
          $('#course_id').val(data.id);
          $('#course_name').val(data.course_name);
          $('#course_alias').val(data.course_alias);
          $('#course_year').val(data.course_year).trigger('change');
          $('#course_year_part').val(data.course_year_part).trigger('change');
          $('#course_lecturer_member_id').val(data.course_lecturer_member_id).trigger('change');
          $('#course_notes').text(data.course_notes);
          $('#course_folder').val(data.course_folder);
          $('#actionType').val("edit");
          $('#course_save_btn').replaceWith('<button type="submit" class="btn btn-success" id="course_save_btn" value="edit">Save Changes <i class="fa fa-save"></i></button>');
          $('#addCourse').modal('show');
        })
      });

      // for delete
        $(document).on('click', '.deleteCourse', function(e){
        e.preventDefault();
        // alert('hello');
        var data_id = $(this).data('id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    "_token": $('input[name=_token]').val(),
                    "id": data_id,
                };
                var url = '{{ route("moderator_setup_course.destroy", ":id") }}';
                url = url.replace(':id', data_id);
                $.ajax({
                    type:"delete",
                    url:url,
                    data:data,
                    success: function(response){
                      // console.log(response);
                        swal(response.message, {
                        icon: "success",
                        })
                        .then((willDelete) => {
                          var oTable = $('#table_course').dataTable();
                          oTable.fnDraw(false);
                        }); 
                    },
                });

                
            } 
        });
      });
    
      // for inserting and updating
      $("#course_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          data: $('#course_form').serialize(),
          url: "{{ route('moderator_setup_course.store') }}",
          type: "POST",
          success: function (data) {
              switch (data.message) {
                case 'successful':
                  $('#addCourse').modal('hide');
                  toastr.success('Process Successfull');
                  var oTable = $('#table_course').dataTable();
                  oTable.fnDraw(false);
                break;
              }
              
          },
          error: function (data) {
             console.log(data.responseText);
              toastr.error('Sorry! error');
              var errormsg = JSON.parse(data.responseText);
              // loop through the 
              swal('Error',errormsg.message,'error')
          }
        });
      });
      
 

  });


  </script>