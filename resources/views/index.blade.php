<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
       <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="{{ asset('assets/images/favicon.png') }}" type="image/png">
      <title>GhIS | LSD</title>
      <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
      <!-- toast -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
   </head>
   <body class="sticky-header">
      <!--Start login Section-->
      <section class="login-section">
         <div class="container">
            <div class="row">
               <div class="login-wrapper">
                  <div class="login-inner">
                     <div class="logo">
                        <img src="{{ asset('assets/images/logo.png') }}"  alt="logo"/>
                     </div>
                     <br>
                     <h2 class="header-title text-center">Login</h2>
                     <form method="GET" action="{{ route('main_dashboard.index') }}"> 
                        @csrf
                        <div class="form-group">
                           <input type="text" class="form-control"  placeholder="Enter Username &hellip;" id="professional_number" name="professional_number" minlength="2"  autofocus  autocomplete="off">
                        </div>
                        <div class="form-group">
                           <input type="password" class="form-control"   id="userPassword" name="userPassword" minlength="3" placeholder="Enter Password &hellip;"  >
                        </div>
                        <div class="form-group">
                           <div class="pull-left">
                              <!-- <div class="checkbox primary">
                                 <input  id="checkbox-2" type="checkbox">
                                 <label for="checkbox-2">Remember me</label>
                              </div> -->
                           </div>
                           <div class="pull-right">
                              <a href="reset-password.html" class="a-link">
                              <i class="fa fa-unlock-alt"></i> Forgot password?
                              </a>
                           </div>
                        </div>
                        <div class="form-group">
                           <input type="hidden" name="mode" id="signInmode" value="login">
                           <button type="submit"  name="submit" id="login"  class="btn btn-primary btn-block">Login <i class="fa fa-sign-in"></i></button>
                        </div>
                        <div class="form-group text-center">
                           Don't have a Trainee account?  <a href="#" style="color: red;">Register </a>
                        </div>
                     </form>
                     <div class="copy-text">
                        <p class="m-0"><?php echo date('Y'); ?> &copy; <a target="_blank" href="http://theprismoid.com/">Prismoidal Company Limited</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--End login Section-->
      <!--Begin core plugin -->
      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
      <!-- End core plugin -->
      <!-- mcafee antivirus -->
      <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
   </body>
</html>

<script type="text/javascript">
   $("#sigin_Form").on("submit",function(e){
       e.preventDefault();
             $.ajax({
             url:"Controllers/users.php",
             method:"POST",
             data:$("#sigin_Form").serialize(),
             beforeSend:function(){  
                   $('#login').text("Loading ...").prop('disabled',true);  
              },
             success:function(data){  
                 console.log(data);
                  if (data == "success") {
                   toastr.success('Login Successfully');
                   window.location.replace("pages/dashboard.php");
                  }
                  else if(data == "error"){
                   toastr.error('Username or Password Incorrect !');
                   $('#sigin_Form')[0].reset();
                   $('#login').text("LOGIN").prop('disabled',false);
                   $('#signInModal').modal('hide');
                  }
                  else if ((data !="success") || (data !="error")) {
                   var fields = data.split('-');
                   var username = fields[0];
                   var passwd = fields[1];

                   $('#chPassUserName').val(username);
                   $('#chPassUserPassword').val(passwd);
                   $('#signInModal').modal('hide');
                   $('#passChModal').modal('show');
                   $('#professional_number').val("");
                   $('#userPassword').val("");
                   $('#login').text("LOGIN").prop('disabled',false);

                  }
             } 

             });  
   });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
// Change password 
$("#changePass_form").on("submit",function(e){
    e.preventDefault();
    $.ajax({
    url:"Controllers/users.php",
    method:"POST",
    data:$("#changePass_form").serialize(),
    beforeSend:function(){ 
      $('#resetPass_btn').text("Loading ...").prop("disabled",true); 
          
    },
    success:function(data){ 
    // console.log(data); 
       if (data == "success") {
         toastr.success('Successfully');
         $("#passChModal").modal("hide");
       }
       else if(data == "error"){
       toastr.error('There was an error');
        $('#newPassword').val("");
        $('#newRetypePassword').val("");
        $('#resetPass_btn').text("Reset Password").prop("disabled",false);

       }
    } 

    });  
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
</script>