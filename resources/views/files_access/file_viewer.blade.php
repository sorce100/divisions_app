<div  id="fileViewerModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <!-- <button type="button" class="close asterick" data-dismiss="modal">&times; CLOSE</button> -->
            <button class="btn btn-danger pull-right" data-dismiss="modal"> <i class="fa fa-remove"></i> </button>
            <h4 class="modal-title">File Viewer</h4>
         </div>
            <div class="modal-body file_viewer_div" style="margin-top: 0;padding: 0;">
               <!-- modal contnet -->
               <!--end modal contnet -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
      </div>
   </div>
</div>

<!-- includes for pdf js -->
<script src="{{ asset('assets/plugins/pdf-viewer/pdf.js') }}"></script>
<script>
    pdfjsLib.workerSrc = "{{ asset('assets/plugins/pdf-viewer/pdf.worker.js') }}";
</script>
<script>
    $(document).ready(function(){
        // for viewing images
        $(document).on('click', '.viewImage', function () {
            const url = $(this).prop('id');
            $('.file_viewer_div').html('<img src="'+url+'" height="100%" width="100%">');
            $("#fileViewerModal").modal("show");
        });
        // for viewing pdf
        $(document).on('click', '.readPdf', function () {
            // get file path
            const url = $(this).prop('id');

            $('.file_viewer_div').html('<div class="top_bar ">'+
            '<button class="btn btnReader" id="prev_page"><i class="fa fa-arrow-circle-left"> </i> Prev Page</button>'+
            '<span class="page-info"> Page <span id="page_num"> </span> of <span id="page_count"> </span> '+
            '<button class="btn btnReader" id="next_page">Next Page <i class="fa fa-arrow-circle-right"></i></button></div>'+
            '<div style="overflow:auto;"><canvas id="pdf_render"></canvas></div>');

            let pdfDoc = null,
                pageNum =1,
                pageIsRendering = false,
                pageNumIsPending = null;

            const scale = 1.3,
                canvas = document.querySelector('#pdf_render'),
                ctx = canvas.getContext('2d');

            // Render the page
            const renderPage = num =>{
            pageIsRendering = true;
            // get the page
            pdfDoc.getPage(num).then(page => {
                // set scale
                const viewport = page.getViewport({scale});
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                const renderCtx = {
                canvasContext: ctx,
                viewport
                }

                page.render(renderCtx).promise.then(() => {
                pageIsRendering = false;

                if(pageNumIsPending !== null){
                    renderPage(pageNumIsPending);
                    pageNumIsPending = null;
                }
                });
                // output current page
                document.querySelector('#page_num').textContent = num;
            });
            };

            // check for pages rendering
            const queueRenderPage = num =>{
            if(pageIsRendering){
                pageNumIsPending = num;
            }else{
                renderPage(num);
            }
            }

            // show Prev Page
            const showPrevPage = () => {
            if(pageNum <= 1){
                return;
            }
            pageNum--;
            queueRenderPage(pageNum);
            }

            // show Next Page
            const showNextPage = () => {
            if(pageNum >= pdfDoc.numPages){
                return;
            }
            pageNum++;
            queueRenderPage(pageNum);
            }

            // Get Document
            pdfjsLib.getDocument(url).promise.then(pdfDoc_ => {
            pdfDoc = pdfDoc_;


            document.querySelector('#page_count').textContent = pdfDoc.numPages;

            renderPage(pageNum);
            })

            // for displaying error when there is no file to render
            .catch(err => {
            // display error
            const div = document.createElement('div');
            div.className = 'error';
            div.appendChild(document.createTextNode(err.message));
            document.querySelector('body').insertBefore(div, canvas);

            // remove top bar
            document.querySelector('.top_bar').style.display = 'none';
            });

            // button events
            document.querySelector('#prev_page').addEventListener('click',showPrevPage);
            document.querySelector('#next_page').addEventListener('click',showNextPage);

            $("#fileViewerModal").modal("show");
        });

    });
    
</script>




