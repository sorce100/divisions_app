<div  id="filesListModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close asterick" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Uploaded List</h4>
         </div>
        <div class="modal-body">
            <!-- modal contnet -->
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <th>File Name</th>
                        <th>Upload Date</th>
                        <th colspan="3" class="text-center">Action</th>
                    </thead>
                    <tbody class="files_list_div"></tbody>
                </table>
            </div>
            <!--end modal contnet -->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        </div>
      </div>
   </div>
</div>
<!-- for file viewer -->
@include('files_access.file_viewer')
