@extends('layouts.header')
@section('pageTitle','Blank')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Lecturer Assessment</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">Lecturer Assessment</h2>
          <hr>
          <div class="table-responsive">
           <table id="" class="tableInit table">
            <thead>
              <th>Lecturer Name</th>
              <th>Course Taught</th>
              <th>Course Level</th>
              <th>Action</th>
            </thead>
            <tbody>
              <tr>
                <td> Surv. Lawyer Harry Anim </td>
                <td>Ethics / Constitution</td>
                <td>1</td>
                <td>
                  <button class="btn btn-primary">Make Assessment <i class="fa fa-male"></i></button>
                </td>
                <tr>
                  <td> Surv. Samuel Oppong Antwi  </td>
                  <td> Mapping / Remote Sensing </td>
                  <td>1</td>
                  <td>
                    <button class="btn btn-primary">Make Assessment <i class="fa fa-male"></i></button>
                  </td>
                </tr>
            </tbody>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
