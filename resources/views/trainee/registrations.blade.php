@extends('layouts.header')
@section('pageTitle','Blank')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Registration</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">Registration</h2>
          <hr>
          <div class="table-responsive">
           <table id="" class="tableInit table">
            <thead>
              <th>Name</th>
              <th>Blank</th>
            </thead>
            <tbody>
              <tr>
                <td>Sorce Kwarteng</td>
                <td>
                  <button class="btn btn-primary">Edit <i class="fa fa-edit"></i></button>
                  <button class="btn btn-danger">Delete <i class="fa fa-trash"></i></button>
                </td>
              </tr>
            </tbody>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
