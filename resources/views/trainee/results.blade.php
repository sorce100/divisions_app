@extends('layouts.header')
@section('pageTitle','Blank')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Results</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
    <div class="col-md-12">
       <div class="white-box feeds">
          <!-- <h2 class="header-title">Feeds</h2> -->
          <div class="feeds-box">
             <div  class="feeds-icon bg-primary"> <i class="fa fa-file-text-o"></i>   </div>
             <div class="feeds-info">
                <h5>Year 1 , First Semester</h5>
                <span>2019/2020 Academic Year</span>
             </div>
          </div>
          <!-- /feed-box-->
          <div class="feeds-box">
             <div  class="feeds-icon bg-primary"> <i class="fa fa-file-text-o"></i>   </div>
             <div class="feeds-info">
                <h5>Year 1 , First Semester</h5>
                <span>2019/2020 Academic Year</span>
             </div>
          </div>
          <!-- /feed-box-->
          <div class="feeds-box">
             <div  class="feeds-icon bg-primary"> <i class="fa fa-file-text-o"></i>   </div>
             <div class="feeds-info">
                <h5>Year 1 , First Semester</h5>
                <span>2019/2020 Academic Year</span>
             </div>
          </div>
          <!-- /feed-box-->
          <div class="feeds-box">
             <div  class="feeds-icon bg-primary"> <i class="fa fa-file-text-o"></i>   </div>
             <div class="feeds-info">
                <h5>Year 1 , First Semester</h5>
                <span>2019/2020 Academic Year</span>
             </div>
          </div>
          <!-- /feed-box-->
          <div class="feeds-box">
             <div  class="feeds-icon bg-primary"> <i class="fa fa-file-text-o"></i>   </div>
             <div class="feeds-info">
                <h5>Year 1 , First Semester</h5>
                <span>2019/2020 Academic Year</span>
             </div>
          </div>
          <!-- /feed-box-->
       </div>
    </div>
   </div>
<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
