@extends('layouts.header')
@section('pageTitle','Blank')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Fees</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->          
   <div class="row">
     <div class="col-md-12">
        <div class="white-box">
         <h2 class="header-title">Fees List</h2>
          <hr>
          <div class="table-responsive">
           <table id="" class="tableInit table">
            <thead>
              <th>Division</th>
              <th>Level</th>
            </thead>
            <tbody>
              <tr>
                <td>Ghislsd</td>
                <td>2</td>
                <td>
                  <button class="btn btn-primary">Make Payment <i class="fa fa-money"></i></button>
                </td>
              </tr>
              <tr>
                <td>Ghislsd</td>
                <td>1</td>
                <td>
                  <button class="btn btn-success">Download Receipt  <i class="fa fa-file"></i></button>
                </td>
              </tr>
            </tbody>
           </table>  
          </div>
         </div>
     </div>
   </div>
<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
