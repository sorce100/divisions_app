@extends('layouts.header')
@section('pageTitle','Trainnee Video Conference Dashboard')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Video Conference Dashboard</h4>
      <div class="clearfix"></div>
   </div>
   <!--End Page Title-->    
    <!-- Create filter  -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
            <h2 class="header-title">Filter</h2>
            </div>
        </div>
    </div>
    <!-- end filter -->
   <div class="row">
       @foreach($conferences as $row)
        <!--  -->
        <div class="col-md-3">
            <div class="card-profile">
                <div class="profile-header">
                <img src="{{ session()->get('member_division_logo') }}"  alt="">
                </div>
                <div class="profile-body">
                    <h3>Host <i class="fa fa-user"></i>: <span class="color">{{ $row->conference_host }}</span></h3><br>
                    <h4>Start Date <i class="fa fa-calendar"></i>: <span class="color">{{ $row->conference_start_date }} </span></h4>
                    <h4>End Date <i class="fa fa-calendar"></i>: <span class="color"> {{ $row->conference_end_date }}</span></h4><br>
                    <br>
                    <hr>
                        <h3 class="color">{{ $row->conference_title }} </h3>
                    <hr>
                <a href="{{ route('conference_room.show', $row->id) }}" class="btn btn-primary btn-sm btn-block m-t-10">Join Conference <i class="fa fa-sign-in"></i></a>
                </div>
            </div>
        </div> 
        <!--  -->
       @endforeach
        <!--  -->
    </div>
<!-- end  -->
</div>
@endSection
<!-- modal for adding new task -->
