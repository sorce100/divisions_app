@extends('layouts.header')
@section('pageTitle','Trainee Dashboard')
@section('pageBody')
<div class="wrapper">
   <!--Start Page Title-->
   <div class="page-title-box">
      <h4 class="page-title">Trainee Dashboard</h4>
      <div class="clearfix"></div>
   </div>
      <!--Start row-->
    <div class="white-box ">
     <div class="row container">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <!-- content goes in here -->
            <a href="{{ route('dashboard.index') }}" class="btn btn-app">
              <span class="badge bg-red">New</span>
              <i class="fa fa-users"></i> All Trainees
            </a>
            <a href="{{ route('trainee_conference.index') }}" class="btn btn-app">
              <i class="fa fa-television"></i> Conferences
            </a>
            <a href="{{ route('profile.index') }}" class="btn btn-app">
              <i class="fa fa-user"></i> Profile
            </a>
            <a href="{{ route('results.index') }}" class="btn btn-app">
              <span class="badge bg-red">New</span>
              <i class="fa fa-file-text-o"></i>Results
            </a>
            <a href="{{ route('registration.index') }}" class="btn btn-app">
              <i class="fa fa-clipboard"></i> Registration
            </a>
            <a href="{{ route('course_materials.index') }}" class="btn btn-app">
              <i class="fa fa-book"></i> Course Material
            </a>
            <a href="{{ route('lecturer_assessment.index') }}" class="btn btn-app">
              <i class="fa fa-male"></i> Lecturer Assessment
            </a>
            <!-- end of body content -->
          </div>
        </div>
      </div>
    </div>
  </div>
   <!--End row-->

   <!-- for students calender -->
   <div class="row">
  <div class="calendar-layout clearfix">      
    <!-- End To Do List-->   
    <div class="col-md-12">
      <div class="white-box">
        <h2 class="header-title">Trainees Calendar</h2>
        <hr>
        <div id='calendar'></div>
      </div>
    </div>
  </div>      
</div>
   <!-- end of student calendar -->
<!-- end  -->
</div>
@endSection

<!-- modal for adding new task -->
