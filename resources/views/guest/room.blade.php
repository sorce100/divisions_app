<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="{{ asset('assets/images/logo.png') }}" type="image/png">
      <title>CPD GUEST ROOM</title>
      <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
   </head>
   <body class="sticky-header">
      <!--Start left side Menu-->
      <div class="left-side sticky-left-side">
         <!--logo-->
         <div class="mainLogo">
            <a href="#">
              <img class="img img-circle mainLogo_img" src="{{ asset('assets/images/logo.png') }}" alt="" >
            </a>
         </div>
         <!--logo-->
         <div class="left-side-inner">
            <!--Sidebar nav-->
            <ul class="nav nav-pills nav-stacked custom-nav">
               <!-- dashboard -->
               <li><a href="{{ route('cpdGuest.index') }}"><i class="fa fa-home"></i> <span>CPD Dashboard</span></a></li>
            </ul>
            <!--End sidebar nav-->
         </div>
      </div>
      <!--End left side menu-->
      <!-- main content start-->
      <div class="main-content" >
      <!-- header section start-->
      <div class="header-section">
         <a class="toggle-btn"><i class="fa fa-bars"></i></a>
         <!--notification menu start -->
         <div class="menu-right">
            <ul class="notification-menu">
               <li>
                  <!-- <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                  <i class="fa fa-envelope"></i>
                  <span class="badge">8</span>
                  </a> -->
                  <div class="dropdown-menu dropdown-menu-head pull-right">
                     <h5 class="title">Messages</h5>
                     <ul class="dropdown-list">
                        <li class="notification-scroll-list notification-list ">
                           <!-- list item-->
                           <a href="javascript:void(0);" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left p-r-10">
                                    <em class="fa  fa-shopping-cart noti-primary"></em>
                                 </div>
                                 <div class="media-body">
                                    <h5 class="media-heading">A new order has been placed.</h5>
                                    <p class="m-0">
                                       <small>29 min ago</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- list item-->
                           <a href="javascript:void(0);" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left p-r-10">
                                    <em class="fa fa-check noti-success"></em>
                                 </div>
                                 <div class="media-body">
                                    <h5 class="media-heading">Databse backup is complete</h5>
                                    <p class="m-0">
                                       <small>12 min ago</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- select 5 -->
                        </li>
                        <li class="last"> <a href="#">View all Messages</a> </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <!-- <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="badge">4</span>
                  </a> -->
                  <div class="dropdown-menu dropdown-menu-head pull-right">
                     <h5 class="title">Notifications</h5>
                     <ul class="dropdown-list normal-list">
                        <li class="message-list message-scroll-list">
                           <a href="#">
                             <span class="photo"> <img src="{{ asset('assets/images/users/avatar-8.jpg') }}" class="img-circle" alt="img"></span>
                             <span class="subject">John Doe</span>
                             <span class="message"> New tasks needs to be done</span>
                             <span class="time">15 minutes ago</span>
                           </a>
                           <a href="#">
                             <span class="photo"> <img src="{{ asset('assets/images/users/avatar-7.jpg') }}" class="img-circle" alt="img"></span>
                             <span class="subject">John Doe</span>
                             <span class="message"> New tasks needs to be done</span>
                             <span class="time">10 minutes ago</span>
                           </a>
                            <!-- select 5 -->
                        </li>
                        <li class="last"> <a  href="#">View all notifications</a> </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ asset('assets/images/logo.png') }}" alt="" />
                  GUEST ACCOUNT
                  <!-- <span class="caret"></span> -->
                  </a>
                  <!-- <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                     <li> <a href="#"> <i class="fa fa-user"></i> Profile </a> </li>
                     <li> <a href="#"> <i class="fa fa-info"></i> Help </a> </li>
                     <li>  
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-lock"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                        </form>
                     </li>
                  </ul> -->
               </li>
            </ul>
         </div>
         <!--notification menu end -->
      </div>
      <!-- header section end-->
      <body>
        <!-- content -->
        <div class="wrapper">
        <!--Start Page Title-->
        <div class="page-title-box">
            <h4 class="page-title" >GHIS LSD CPD ROOM</h4>
            <div class="clearfix"></div>
        </div>
        <!--End Page Title-->          
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div id="jitsi_meet_div"></div>
                </div>
            </div>
        </div>
        <!-- end  -->
        </div>
        <!-- end of content -->
      </body>
      <!--Start  Footer -->
      <footer class="footer-main"> {{date('Y')}}  &copy; <a target="_blank" href="http://theprismoid.com/">Prismoidal Company Limited</a>  </footer>
      <!--End footer -->
      </div>
      <!--End main content -->
      <!--Begin core plugin -->
      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
      <script  src="{{ asset('assets/js/jquery.slimscroll.js ') }}"></script>
      <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
      <script src="{{ asset('assets/js/functions.js') }}"></script>
      <!-- End core plugin -->
      <script src="https://meet.jit.si/external_api.js"></script>
        <script type="text/javascript">
            const domain = "lisagonline.info";
            const options = {
            roomName: "{{$conferenceDetails}}",
            width: '100%',
            height: 750,
            parentNode: document.querySelector("#jitsi_meet_div"),
            };
            const api = new JitsiMeetExternalAPI(domain, options);
        </script>