<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
                        'member_first_name',
                        'member_middle_name',
                        'member_last_name',
                        'member_dob',
                        'member_tel',
                        'member_emergency_tel',
                        'member_email',
                        'member_region',
                        'member_image',
                        'member_folder',
                        'member_address',
                        'member_location',
                        'member_professional_number',
                        'member_type',
                        'member_designation_id',
                        'member_company_name',
                        'member_company_type',
                        'member_company_tel',
                        'member_company_email',
                        'member_company_region',
                        'member_office_address',
                        'member_office_location',
                        'member_notes',
                        'updated_member_id',
                        'record_hide',
                        'division_id'
                    ];
}
