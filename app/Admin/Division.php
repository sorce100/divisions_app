<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = ['division_name','division_alias','division_contact_fname','division_contact_lname','division_contact_tel','division_address','division_location','division_status_log','division_logo','division_folder','division_notes','updated_member_id','record_hide'];
}
