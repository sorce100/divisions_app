<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
    protected $fillable = ['member_type_name', 'member_type_notes','division_id','updated_member_id','record_hide'];
}
