<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class VideoConference extends Model
{
    protected $fillable = [
        'conference_title', 
        'conference_host', 
        'conference_target', 
        'conference_start_date', 
        'conference_end_date', 
        'conference_room_name', 
        'conference_notes', 
        'division_id',  
        'updated_member_id',
        'record_hide'
    ];
}
