<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['course_name', 'course_alias','course_year','course_year_part','course_lecturer_member_id','course_notes','course_folder','updated_member_id','course_division_id','record_hide'];
}
