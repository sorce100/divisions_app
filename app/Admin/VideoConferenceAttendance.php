<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class VideoConferenceAttendance extends Model
{
    protected $fillable = ['attendance_member_type', 'attendance_member_id', 'attendance_conference_id', 'attendance_division_id'];
}
