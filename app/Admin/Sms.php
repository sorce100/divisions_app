<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $fillable = ['sms_reciever_member_type', 'sms_reciever_member_id','sms_group_number','sms_reference_type','sms_destination_tel','sms_send_message','sms_response','division_id','sender_member_id'];

    // sms sending array
    public function send_sms($smsDestination_tel,$smsMessage,$smsReferenceType,$smsGroupNumber,$smsReceiverMemberType,$smsReceiverMemberId){
        $attributes=[];
        $url ="http://206.225.81.36/ucm_api/index.php"; 
        $request_id = "UCM _" . preg_replace('/\D/', '', date('Y-m-d H:i:s'));
        $smsReference = date('Y-m-d H:i:s');
        $data = [  
            'reference' => $smsReference, // your own reference
            'message_type' => "1",
            'message' => $smsMessage, 
            'user_id' => "313", 
            'app_id' => "600157", 
            'org_id' => "139", 
            'src_address' => 'GHIS LSD', //not more than 11 characters
            'dst_address' => $smsDestination_tel,
            'service_id' => "", 
            'operation' => "send", 
            'timestamp' => preg_replace('/\D/', '', date("YYYYmmddHHiiss")), 
            'auth_key' => md5(600157 . date("YYYYmmddHHiiss") . "!QAZ2wsx*")  
        ];
            $curl = curl_init($url); 
            curl_setopt($curl, CURLOPT_HEADER, false); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
            curl_setopt($curl, CURLOPT_POST, true); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            $ucm_response = curl_exec($curl);

            if ($ucm_response) {
                // $ucm_response = "'".$ucm_response."'";
                $response = json_decode($ucm_response,true);
                $attributes['sms_reciever_member_type'] = $smsReceiverMemberType ;
                $attributes['sms_reciever_member_id'] = $smsReceiverMemberId ;
                $attributes['sms_reference_type'] = $smsReferenceType ;
                $attributes['sms_group_number'] = $smsGroupNumber ;
                $attributes['sms_destination_tel'] = $smsDestination_tel ;
                $attributes['sms_send_message'] = $smsMessage ;
                $attributes['sms_response'] = $response['desc'] ;
                $attributes['division_id'] = session()->get('member_division_id');
                $attributes['sender_member_id'] = session()->get('member_id') ; 
                
                $this::create($attributes);
            }


    }



}
