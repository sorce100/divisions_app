<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['group_division_id', 'group_name','group_module_id','updated_member_id','record_hide'];
}
