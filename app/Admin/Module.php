<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['module_name', 'module_url','module_category','module_division_id','updated_member_id','record_hide'];
}
