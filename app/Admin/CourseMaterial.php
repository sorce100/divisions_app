<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class CourseMaterial extends Model
{
    protected $fillable = [
        'course_material_course_id',
        'course_material_course_folder',
        'course_material_upload_title',
        'course_material_target',
        'course_material_files',
        'course_material_notes',
        'updated_member_id',
        'course_division_id',
        'record_hide'
    ];
}
