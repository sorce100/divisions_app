<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class MemberDesignation extends Model
{
    protected $fillable = ['member_designation_name', 'member_designation_notes','division_id','updated_member_id','record_hide'];
}
