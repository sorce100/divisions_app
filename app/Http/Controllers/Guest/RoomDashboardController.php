<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Admin\VideoConference;
use App\Http\Controllers\Controller;

class RoomDashboardController extends Controller
{
    public function index()
    {
        $conferences = VideoConference::latest()->where('record_hide','NO')->where('conference_target','Guest')->where('division_id',1)->get();
        return view('guest.room_dashboard',compact('conferences'));
    }
}
