<?php

namespace App\Http\Controllers\Member\Conference;

use Illuminate\Http\Request;
use App\Admin\VideoConference;
use App\Http\Controllers\Controller;
use App\Admin\VideoConferenceAttendance;

class ConferenceRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attendanceAttr = [];
        // find conference room name and send to blade
        $conferenceDetails = VideoConference::findorFail($id);
        $conferenceDetails = $conferenceDetails->conference_room_name;
        // get attendance details and save
        $attendanceAttr['attendance_member_type'] = session()->get('member_type');
        $attendanceAttr['attendance_member_id'] = session()->get('member_id');
        $attendanceAttr['attendance_division_id'] = session()->get('member_division_id');
        $attendanceAttr['attendance_conference_id'] = $id;
        // save attendance
        VideoConferenceAttendance::create($attendanceAttr);
        // return view
        return view('member.conference.room',compact('conferenceDetails'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
