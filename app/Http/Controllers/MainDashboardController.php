<?php

namespace App\Http\Controllers;

use App\Admin\Group;
use App\Admin\Member;
use App\Admin\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MainDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {
        $userModulesUrls = [];
        $user = Auth::user();
        // Session::put('variableName', $value);
        $member = Member::findorFail($user->user_member_id);
        $division = Division::findorFail($user->user_division_id);
        // get member fullname
        $member_fullName = $member->member_first_name.' '.$member->member_last_name;
        $member_folder_name = $member->member_folder.'/'.$member->member_image;
        $member_image = url('/uploads/divisions/'.$division->division_folder.'/members'.'/'.$member_folder_name);
        $member_division_logo = url('/uploads/divisions/'.$division->division_folder.'/'.$division->division_logo);
        // get all user groups modules
        $groupModulesIds = DB::table('groups')
                                ->select('group_module_id')
                                ->where('id', $user->user_group_id)
                                ->where('record_hide','NO')
                                ->first();
        // Decode The modules Ids
        $groupModulesIds = json_decode($groupModulesIds->group_module_id);
        // get modules object into array
        if(!empty($groupModulesIds)){
            foreach($groupModulesIds as $moduleId){
                // get the module url for each
                $userModulesUrls[] = DB::table('modules')
                                        ->select('module_url')
                                        ->where('id', $moduleId)
                                        ->where('record_hide','NO')
                                        ->first();
    
            }
        }
        else{
            $userModulesUrls = 'null';
        }
        
        // dd($userModulesUrls);
        // dd($groupModulesIdsObject['group_module_id']);
        // setting sessions
        session([
            'member_id' => $member->id,
            'member_type' => $member->member_type,
            'member_fullName' => $member_fullName,
            'member_image' => $member_image,
            'user_account_type' => $user->user_type,
            'member_folder_name' => $member_folder_name,
            'member_division_id' => $division->id,
            'member_division_name' => $division->division_name,
            'member_division_folder' => $division->division_folder,
            'member_division_logo' => $member_division_logo,
            'member_modules_urls' => $userModulesUrls,
            ]);
        //////////////////////////////////////////////////
        return view('member.dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
