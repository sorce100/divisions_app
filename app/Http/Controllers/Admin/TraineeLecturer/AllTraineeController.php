<?php

namespace App\Http\Controllers\Admin\TraineeLecturer;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AllTraineeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('users')->select('users.id','users.user_type','users.user_account_status_log',
                    'members.member_email','members.member_tel','members.member_region',
                    DB::raw("CONCAT(members.member_first_name,' ',members.member_middle_name,' ',members.member_last_name) AS member_full_name")
                    )
            ->leftJoin('members','users.user_member_id','=','members.id')
            ->where('users.user_type','Trainee')
            ->where('members.division_id',session()->get('member_division_id'))->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('user_type', function($row){
                        switch ($row->user_type) {
                            case 'Trainee':
                                $btn = '<button type="button" class="btn btn-primary round">'.$row->user_type.'</button>';
                            break;
                            default:
                                $btn = '<button type="button" class="btn btn-danger round">'.$row->user_type.'</button>';
                            break;
                        }
                        return $btn;
                    })
                    ->rawColumns(['user_type'])
                    ->make(true);
        }
        return view('admin.lecturers.allTrainees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
