<?php

namespace App\Http\Controllers\Admin\TraineeLecturer;

use DataTables;
use App\Admin\Course;
use Illuminate\Http\Request;
use App\Admin\CourseMaterial;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CourseMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            // get data of attendence
            $data = DB::table('course_materials')
                    ->select(
                    'course_materials.id',
                    'course_materials.course_material_target',
                    'course_materials.course_material_files',
                    'course_materials.updated_at',
                    DB::raw('CONCAT(members.member_first_name," ",members.member_last_name) AS full_name'),
                    'courses.course_alias',
                    'courses.course_year',
                    'courses.course_year_part'
                    )
                    ->leftJoin('members','course_materials.updated_member_id','=','members.id')
                    ->leftJoin('courses','course_materials.course_material_course_id','=','courses.id')
                    ->where('course_materials.record_hide','NO')
                    ->where('courses.course_lecturer_member_id',session()->get('member_id'))
                    ->where('course_materials.course_division_id',session()->get('member_division_id'))->get();

                    return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCourseMat">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCourseMat">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->addColumn('files_total', function($row){
                        $totalFiles = sizeof(json_decode($row->course_material_files));
                        $btn = "<button type='button' class='btn btn-info listUploadedFiles' id='$row->id'>
                                    <span class='btn-label'><i class='fa fa-folder'></i></span>$totalFiles
                                </button>";
                        return $btn;
                    })
                    ->rawColumns(['action','files_total'])
                    ->make(true);
        }
        // get course name assigned to the lecturer
        $courseNames = Course::select('id','course_name','course_folder')->where('course_division_id',session()->get('member_division_id'))->where('record_hide','NO')->where('course_lecturer_member_id',session()->get('member_id'))->get();
        return view('admin.lecturers.courseMaterial.manage',compact('courseNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'course_material_course_id' => 'required|integer|min:1',
            'course_material_course_folder' => 'required|string|min:3',
            'course_material_upload_title' => 'required|string|min:3',
            'course_material_target' => 'required|string|min:3',
            'course_material_files.*' => 'mimes:doc,pdf,docx,xls,xlsx,ppt,pptx,png,jpeg,jpg|max:2048',
            'course_material_notes' => 'string|min:3',
        ]);
        // after validation add the defaults
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['course_division_id'] = session()->get('member_division_id');
        $attributes['record_hide'] ='NO';

        // check if uploads has files
        if($request->hasfile('course_material_files')){
            foreach($request->file('course_material_files') as $file)
            {
                $fileName = date('dmYH').mt_rand(2, 50).'.'.$file->extension();
                $file_path = public_path('uploads/divisions/'.session()->get('member_division_folder').'/'.'course_materials/'.$request->course_material_course_folder.'/');
                $file->move($file_path,$fileName);  
                if(file_exists($file_path.$fileName)){
                    $fileNames[] = $fileName;
                }  
            }
        }
        
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                // add new upload files to attributes
                $attributes['course_material_files'] = json_encode($fileNames);
                CourseMaterial::create($attributes);
            break;
            case 'edit':
                // check if uploaded files have being deleted
                foreach(json_decode($request->uploaded_course_material_files_array) as $uploadedFile){
                    if(!in_array($uploadedFile ,$request->old_course_material_files )){
                        unlink(public_path('uploads/divisions/'.session()->get('member_division_folder').'/'.'course_materials/'.$request->course_material_course_folder.'/'.$uploadedFile));
                    }
                }
                // if new files are uploaded then merge the new and old and save
                if(!empty($fileNames)){
                    $attributes['course_material_files'] = json_encode(array_merge($fileNames,$request->old_course_material_files));
                }
                else{
                    $attributes['course_material_files'] = json_encode($request->old_course_material_files);
                }
                CourseMaterial::findorFail($request->input('course_material_id'))->update($attributes);
            break;
        }
        // return with success
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = "";
        $rows = CourseMaterial::findorFail($id);
        $filesDecode = json_decode($rows->course_material_files);
        foreach($filesDecode as $file_name){
            // filepath
            $file_path = url('uploads/divisions/'.session()->get('member_division_folder').'/'.'course_materials/'.$rows->course_material_course_folder.'/'.$file_name);
            $download_path = public_path('uploads/divisions/'.session()->get('member_division_folder').'/'.'course_materials/'.$rows->course_material_course_folder.'/'.$file_name);
            $data .= "<tr>";
            // file name and upload date
            $data .= "<td>$file_name</td><td>$rows->updated_at</td>";
            $fname = explode('.', $file_name);
            //get file extension 
            switch($fname[1]){
                case 'pdf':
                    $data .= "<td><button class='btn btn-info readPdf' id='$file_path'>View File <i class='fa fa-book'></i></button></td>";
                break;
                case 'jpeg':
                    $data .= "<td><button class='btn btn-info viewImage' id='$file_path'>View File <i class='fa fa-picture-o'></i></button></td>";
                break;
                case 'jpg':
                    $data .= "<td><button class='btn btn-info viewImage' id='$file_path'>View File <i class='fa fa-picture-o'></i></button></td>";
                break;
                case 'png':
                    $data .= "<td><button class='btn btn-info viewImage' id='$file_path'>View File <i class='fa fa-picture-o'></i></button></td>";
                break;
                default:
                    $data .="<td></td>";
            }
            // $data .= "<td><button class='btn btn-primary downloadFile' id='$download_path'>Download File <i class='fa fa-download'></i></button></td></tr>";
            $data .= "<td><a href='/files_download?file_path=$download_path' data-id='$rows->id' class='btn btn-primary' > Download File <i class='fa fa-download'></i></a></tr>";
        }
        // return response()->json(['message'=>'successful']);
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courseMaterials = CourseMaterial::find($id);
        return response()->json($courseMaterials);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
        $attributes['id']= $id;
        $attributes['record_hide']= 'YES';
        $attributes['updated_member_id'] = session()->get('member_id');
        
        CourseMaterial::findorFail($id)->update($attributes);

        return response()->json(['message'=>'Successful']);
    }
}
