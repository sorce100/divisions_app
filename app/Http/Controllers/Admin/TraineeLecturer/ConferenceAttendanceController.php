<?php

namespace App\Http\Controllers\Admin\TraineeLecturer;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ConferenceAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get data of attendence
            $data = DB::table('video_conference_attendances')
                    ->select(
                    'video_conference_attendances.id',
                    'video_conference_attendances.updated_at',
                    DB::raw('CONCAT(members.member_first_name," ",members.member_last_name) AS full_name'),
                    'members.member_type',
                    'video_conferences.conference_room_name'
                    )
                    ->leftJoin('members','video_conference_attendances.attendance_member_id','=','members.id')
                    ->leftJoin('video_conferences','video_conference_attendances.attendance_conference_id','=','video_conferences.id')
                    ->where('video_conferences.updated_member_id',session()->get('member_id'))
                    ->where('video_conference_attendances.attendance_division_id',session()->get('member_division_id'))->get();

                    return Datatables::of($data)->addIndexColumn()->make(true);
        }

        return view('admin.lecturers.conferenceAttendance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
