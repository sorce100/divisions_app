<?php

namespace App\Http\Controllers\Admin\Users;

use App\User;
use DataTables;
use App\Admin\Sms;
use App\Admin\Group;
use App\Admin\Member;
use App\Admin\Division;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $division_id=1;

        if ($request->ajax()) {
            $division_id = 1;
            // get division
            $data = User::latest()->where('record_hide','NO')->where('user_division_id',$division_id)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editUser">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteUser">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        // 
        $members = Member::latest()->where('record_hide','NO')->where('division_id',$division_id)->get();
        $divisions = Division::latest()->where('record_hide','NO')->where('id',$division_id)->get();
        $groups = Group::latest()->where('record_hide','NO')->where('group_division_id',$division_id)->get();
        
        return view('admin.users.manage',compact('members','divisions','groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get raw password before its being hashed
        $unhasedPassword = $request->input('password');
        $accountType = $request->input('user_type');
        // validate entry
        $attributes = request()->validate([
            'user_division_id' => 'required|numeric|min:1',
            'user_type' => 'required|string|min:3',
            'user_member_id' => 'required|numeric|min:1',
            'email' => 'required|string|min:3',
            'password' => 'required|string|min:3',
            'user_group_id' => 'required|numeric|min:1',
            'user_login_status_log' => 'required|string|min:3',
            'user_account_status_log' => 'required|string|min:3',
            'user_notes' => 'required|string|min:3',
        ]);
        // after validation add the defaults
        $attributes['password'] = bcrypt($request->input('password'));
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['record_hide'] ='NO';
        // get member account details to send sms with their numbers
        $member_details = Member::find($request->input('user_member_id'));
        // get member phone number
        $smsDestination_tel = $member_details->member_tel;
        // replace @ symbol with html entities
        $smsEmail = str_replace("@",'&commat;',$member_details->member_email);
        $smsEmail = $member_details->member_email;
        $smsDestinationFullName = $member_details->member_first_name.' '.$member_details->member_last_name;
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                // sms group number
                $smsGroupNumber = date('dmY').'-'.mt_rand(1,9100);
                // sms content message
                $smsMessage = "Dear $smsDestinationFullName, your GhIS $accountType account has being created successfully. Please log on https://ghislsd.com with credentials, email: $smsEmail and password : $unhasedPassword";
                // sending user credentials sms
                $smsObj = new Sms;
                $smsObj->send_sms($smsDestination_tel,$smsMessage,'Single',$smsGroupNumber,$request->input('user_type'),$request->input('user_member_id'));
                // create user credentials
                User::create($attributes);
                // update member upon account creating
                

               
            break;
            case 'edit':
                User::findorFail($request->input('user_id'))->update($attributes);
            break;
        }
        
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
        $attributes['id']= $id;
        $attributes['record_hide']= 'YES';
        $attributes['updated_member_id'] =1;
        
        Users::findorFail($id)->update($attributes);

        return response()->json(['message'=>'Successful']);
    }
}
