<?php

namespace App\Http\Controllers\Admin\TraineeModerator;

use DataTables;
use App\Admin\Course;
use App\Admin\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SetupCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get division
            $data = DB::table('courses')->select(
                    'courses.id',
                    'courses.course_name',
                    'courses.course_alias',
                    'courses.course_year',
                    'courses.course_year_part',
                    'courses.updated_at',
                    DB::raw("CONCAT(members.member_first_name,' ',members.member_last_name) AS full_name")
                    )
                    ->leftJoin('members','courses.course_lecturer_member_id','=','members.id')
                    ->where('courses.record_hide','NO')
                    ->where('courses.course_division_id',session()->get('member_division_id'))->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCourse">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCourse">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        // set all members of the divison for modal
        $members = Member::latest()->where('division_id',session()->get('member_division_id'))->where('record_hide','NO')->where('member_type',1)->get();
        return view('admin.traineemoderator.setupCourse.manage',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'course_name' => 'required|string|min:3',
            'course_alias' => 'required|string|min:3',
            'course_year' => 'required|string|min:3',
            'course_year_part' => 'required|string|min:3',
            'course_lecturer_member_id' => 'required|numeric|min:3',
        ]);
        // after validation add the defaults
        $attributes['course_division_id'] = session()->get('member_division_id');
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                //create the course folder
                if (!empty($request->course_alias)) {
                    $request->course_folder = $request->course_alias.mt_rand(1, 100);
                }
                // check for division folder
                $file_path = public_path('uploads/divisions/'.session()->get('member_division_folder').'/'.'course_materials/'.$request->course_folder);
                if(!file_exists($file_path)){
                    mkdir($file_path, 0777, true);
                }
                // add folder name in attributes
                $attributes['course_folder'] = $request->course_folder;
                // save in database
                Course::create($attributes);
            break;
            case 'edit':
                $attributes['course_folder'] = $request->course_folder;
                Course::findorFail($request->input('course_id'))->update($attributes);
            break;
        }
        
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findorFail($id);
        return response()->json($course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
       $attributes['id']= $id;
       $attributes['record_hide']= 'YES';
       $attributes['updated_member_id'] =session()->get('member_id');
       Course::findorFail($id)->update($attributes);
       return response()->json(['message'=>'Successful']);
    }
}
