<?php

namespace App\Http\Controllers\Admin\Division;

use DataTables;
use App\Admin\Division;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get division
            $data = Division::latest()->where('record_hide','NO')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('logoImage', function($row){
                        // $url = public_path('uploads/divisions/'.$row->division_folder.'/'.$row->division_logo);
                        $url = url('/uploads/divisions/'.$row->division_folder.'/'.$row->division_logo);
                        $img = "<img src='$url' width='70px' height='70px' >";
                        return $img;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editDivision">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteDivision">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action','logoImage'])
                    ->make(true);
        }

        return view('admin.division.manage');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $attributes = request()->validate([
            'division_name' => 'required|string|min:3',
            'division_alias' => 'required|string|min:3',
            'division_contact_fname' => 'required|string|min:3',
            'division_contact_lname' => 'required|string|min:3',
            'division_contact_tel' => 'required|string|max:20',
            'division_address' => 'string',
            'division_location' => 'string',
            'division_status_log' => 'required|string',
            'division_notes' => 'string',
        ]);

        // after validation add the defaults
        $attributes['updated_member_id'] =1;
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                // check if it has a file or not
                if($request->hasFile('division_logo')){
                    // validate if there is file upload
                    request()->validate(['division_logo' => 'image|mimes:jpeg,png,jpg',]);

                    if (empty($request->division_folder)) {
                        $request->division_folder = $request->division_alias;
                    }
                    // check for division folder
                    $file_path = public_path('uploads/divisions/'.$request->division_folder);
                    if(!file_exists($file_path)){
                        mkdir($file_path, 0777, true);
                    }
                    // grab the uploaded file from the request facades
                    $request->division_logo = $request->file('division_logo');
                    // add the time to the extension of the image uploaded by client
                    $filename = date('dmYi').".".$request->division_logo->getClientOriginalExtension();
                    // now use image intervention installed earlier to resize and save profile picture
                    Image::make($request->division_logo)->resize(250,250)->save($file_path.'/'.$filename);
                    // if succefful add logo and folder name
                    $attributes['division_logo'] = $filename;
                    $attributes['division_folder'] = $request->division_folder;
                }
                Division::create($attributes);
            break;
            case 'edit':
                // when 
                if($request->hasFile('division_logo')){
                    // validate if there is file upload
                    request()->validate(['division_logo' => 'image|mimes:jpeg,png,jpg',]);

                    if (empty($request->division_folder)) {
                        $request->division_folder = $request->division_alias;
                    }
                    // check for division folder
                    $file_path = public_path('uploads/divisions/'.$request->division_folder);
                    if(!file_exists($file_path)){
                        mkdir($file_path, 0777, true);
                    }
                    // grab the uploaded file from the request facades
                    $request->division_logo = $request->file('division_logo');
                    // remove the file from division folder
                    unlink($file_path.'/'.$request->old_division_logo);
                    // add the time to the extension of the image uploaded by client
                    $filename = date('dmYi').".".$request->division_logo->getClientOriginalExtension();
                    // now use image intervention installed earlier to resize and save profile picture
                    Image::make($request->division_logo)->resize(150,150)->save($file_path.'/'.$filename);
                    // if succefful add logo and folder name
                    $attributes['division_logo'] = $filename;
                    $attributes['division_folder'] = $request->division_folder;
                }
                Division::findorFail($request->input('division_id'))->update($attributes);
            break;
        }
        
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $division = Division::find($id);
        $url = url('/uploads/divisions/'.$division->division_folder.'/'.$division->division_logo);
        $division['logoImage'] = "<img src='$url' width='100%' height='200px' >";
        return response()->json($division);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
        $attributes['id']= $id;
        $attributes['record_hide']= 'YES';
        $attributes['updated_member_id'] = session()->get('member_id');
        
        Division::findorFail($id)->update($attributes);

        return response()->json(['message'=>'Successful']);
    }
}
