<?php

namespace App\Http\Controllers\Admin\Module;

use DataTables;
use App\Admin\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class manageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get division
            $data = DB::table('modules')->select('modules.id','modules.module_name','modules.module_category','modules.updated_at','divisions.division_alias')
            ->leftJoin('divisions','modules.module_division_id','=','divisions.id')
            ->where('modules.record_hide','NO')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editModule">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteModule">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        // get divisions
        $divisions = DB::table('divisions')->select('id','division_alias')->get();

        return view('admin.module.manage',compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'module_division_id' => 'required|numeric',
            'module_name' => 'required|min:3',
            'module_category' => 'required|min:3',
            'module_url' => 'required|min:3'
        ]);
        // after validation add the defaults
        $attributes['updated_member_id'] =1;
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                // only division that creates can modify
                $attributes['module_division_id'] =1;
                Module::create($attributes);
            break;
            case 'edit':
                Module::findorFail($request->input('module_id'))->update($attributes);
            break;
        }
        
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::find($id);
        return response()->json($module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
        $attributes['id']= $id;
        $attributes['record_hide']= 'YES';
        $attributes['updated_member_id'] = session()->get('member_id');
        
        Module::findorFail($id)->update($attributes);

        return response()->json(['message'=>'Successful']);
    }
}
