<?php

namespace App\Http\Controllers\Admin\Member;

use DataTables;
use App\Admin\Member;
use App\Admin\MemberType;
use Illuminate\Http\Request;
use App\Admin\MemberDesignation;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get members
            $data = $data = DB::table('members')->select('*',
                DB::raw("CONCAT(member_first_name,' ',member_last_name) AS member_full_name")
                )
            ->where('division_id',session()->get('member_division_id'))->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('memberImage', function($row){
                        switch($row->member_folder){
                            case 'NONE':
                            $url = session()->get('member_division_logo');
                            break;
                            default:
                            $url = url('/uploads/divisions/'.session()->get('member_division_folder').'/members/'.$row->member_folder.'/'.$row->member_image);
                        }
                        $img = "<img src='$url' class='viewImage' id='$url'  width='70px' height='70px' >";
                        return $img;
                    })
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editMember">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteMember">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action','memberImage'])
                    ->make(true);
        }
        // get the member types and designation
        $memberDesignations = MemberDesignation::latest()->where('record_hide','NO')->where('division_id',session()->get('member_division_id'))->get();
        $memberTypes = MemberType::latest()->where('record_hide','NO')->where('division_id',session()->get('member_division_id'))->get();
        // return view and data
        return view('admin.member.manage',compact(['memberDesignations','memberTypes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'member_first_name' => 'required|string|min:3',
            'member_middle_name' => 'nullable|string|min:3',
            'member_last_name' => 'required|string|min:3',
            'member_dob' => 'string|min:3',
            'member_tel' => 'required|numeric|min:10',
            'member_emergency_tel' => 'numeric|min:10',
            'member_email' => 'required|email|min:3',
            'member_region' => 'required|string|min:3',
            'member_address' => 'required|string|min:3',
            'member_location' => 'required|string|min:3',
            'member_professional_number' => 'required|integer|min:3',
            'member_type' => 'required|string|min:1',
            'member_designation_id' => 'required|string|min:1',
            'member_company_name' => 'required|string|min:3',
            'member_company_type' => 'required|string|min:3',
            'member_company_tel' => 'required|numeric|min:10',
            'member_company_email' => 'required|email|min:8',
            'member_company_region' => 'required|string|min:3',
            'member_office_address' => 'string|min:3',
            'member_office_location' => 'string|min:3',
            'member_notes' => 'string|min:3',
        ]);

        // after validation add the defaults
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['division_id'] = session()->get('member_division_id');
        $request->division_folder ='sdfg';
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                // check if it has a file or not
                if($request->hasFile('member_image')){
                    // validate if there is file upload
                    request()->validate(['member_image' => 'image|mimes:jpeg,png,jpg',]);

                    if (empty($request->member_folder)) {
                        $request->member_folder = $request->member_last_name.mt_rand(1, 100);
                    }
                    // check for division folder
                    $file_path = public_path('uploads/divisions/'.$request->division_folder.'/'.'members/'.$request->member_folder);
                    if(!file_exists($file_path)){
                        mkdir($file_path, 0777, true);
                    }
                    // grab the uploaded file from the request facades
                    $request->member_image = $request->file('member_image');
                    // add the time to the extension of the image uploaded by client
                    $filename = date('dmYi').".".$request->member_image->getClientOriginalExtension();
                    // now use image intervention installed earlier to resize and save profile picture
                    Image::make($request->member_image)->resize(250,250)->save($file_path.'/'.$filename);
                    // if succefful add logo and folder name
                    $attributes['member_image'] = $filename;
                    $attributes['member_folder'] = $request->member_folder;
                }
                else{
                    // if no image is uploaded
                    $attributes['member_image'] = "NONE";
                    $attributes['member_folder'] = "NONE";
                }
                Member::create($attributes);
            break;
            case 'edit':
                // check if it has a file or not
                if($request->hasFile('member_image')){
                    // validate if there is file upload
                    request()->validate(['member_image' => 'image|mimes:jpeg,png,jpg',]);
                    if ($request->member_folder=='NONE') {
                        $request->member_folder = $request->member_last_name.mt_rand(1, 100);
                    }
                    // check for division folder
                    $file_path = public_path('uploads/divisions/'.$request->division_folder.'/'.'members/'.$request->member_folder);
                    if(!file_exists($file_path)){
                        mkdir($file_path, 0777, true);
                    }
                    // clear contents od the folder if it already exits
                    array_map( 'unlink', array_filter((array) glob($file_path."/*") ) );
                    // grab the uploaded file from the request facades
                    $request->member_image = $request->file('member_image');
                    // add the time to the extension of the image uploaded by client
                    $filename = date('dmYi').".".$request->member_image->getClientOriginalExtension();
                    // now use image intervention installed earlier to resize and save profile picture
                    Image::make($request->member_image)->resize(250,250)->save($file_path.'/'.$filename);
                    // if succefful add logo and folder name
                    $attributes['member_image'] = $filename;
                    $attributes['member_folder'] = $request->member_folder;
                }
                else{
                    // if no image is uploaded
                    $attributes['member_image'] = $request->old_member_image;
                    $attributes['member_folder'] = $request->member_folder;
                }

                Member::findorFail($request->input('member_id'))->update($attributes);
            break;
        }
        
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        switch($member->member_folder){
            case 'NONE':
            $url = session()->get('member_division_logo');
            break;
            default:
            $url = url('/uploads/divisions/'.session()->get('member_division_folder').'/members/'.$member->member_folder.'/'.$member->member_image);
        }
        $member['memberImage'] = "<img src='$url' width='100%' height='200px' >";
        return response()->json($member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
        $attributes['id']= $id;
        $attributes['record_hide']= 'YES';
        $attributes['updated_member_id'] = session()->get('member_id');
        
        Member::findorFail($id)->update($attributes);

        return response()->json(['message'=>'Successful']);
    }
}
