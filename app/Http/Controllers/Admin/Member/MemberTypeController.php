<?php

namespace App\Http\Controllers\Admin\Member;

use DataTables;
use App\Admin\MemberType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = MemberType::latest()->where('record_hide','NO')
                    ->where('division_id',session()->get('member_division_id'))
                    ->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editMemberType">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteMemberType">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.member.member_type');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'member_type_name' => 'required|string|min:3',
            'member_type_notes' => 'string',
        ]);
        // after validation add the defaults
        $attributes['division_id'] = session()->get('member_division_id');
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                MemberType::create($attributes);
            break;
            case 'edit':
                MemberType::findorFail($request->input('member_type_id'))->update($attributes);
            break;
        }
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $memberTypes = MemberType::find($id);
        return response()->json($memberTypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
       $attributes['id']= $id;
       $attributes['record_hide']= 'YES';
       $attributes['updated_member_id'] = session()->get('member_id');
       
       MemberType::findorFail($id)->update($attributes);

       return response()->json(['message'=>'Successful']);
    }
}
