<?php

namespace App\Http\Controllers\Admin\VideoConference;

use DataTables;
use Illuminate\Http\Request;
use App\Admin\VideoConference;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get conference
            $data = VideoConference::latest()->where('record_hide','NO')->where('division_id',session()->get('member_division_id'))->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editConference">Edit <i class="fa fa-edit"></i></a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteConference">Delete <i class="fa fa-trash"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('admin.conference.manage');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'conference_title' => 'required|string|min:3',
            'conference_host' => 'required|string|min:3',
            'conference_target' => 'required|string|min:3',
            'conference_start_date' => 'required|string|min:10',
            'conference_end_date' => 'required|string|min:10',
            'conference_room_name' => 'required|string|min:3',
            'conference_notes' => 'string|min:3',
        ]);
        // after validation add the defaults
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['division_id'] = session()->get('member_division_id');
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                VideoConference::create($attributes);
            break;
            case 'edit':
                VideoConference::findorFail($request->input('conference_id'))->update($attributes);
            break;
        }
        
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $videoconference = VideoConference::find($id);
        return response()->json($videoconference);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // hide record instead of deleting in case is a foreign key in another table
        $attributes['id']= $id;
        $attributes['record_hide']= 'YES';
        $attributes['updated_member_id'] =1;
        
        VideoConference::findorFail($id)->update($attributes);

        return response()->json(['message'=>'Successful']);
    }
}
