<?php

namespace App\Http\Controllers\Admin\Sms;

use DataTables;
use App\Admin\Sms;
use App\Admin\Member;
use App\Admin\MemberType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // get division
            // $data = Sms::latest()->where('division_id',session()->get('member_division_id'))->get();

            $data = DB::table('sms')->select(
                'sms.id',
                'sms.sms_reciever_member_type',
                'sms.sms_reciever_member_id',
                'sms.sms_destination_tel',
                'sms.sms_reference_type',
                'sms.sms_send_message',
                'sms.sms_response',
                'sms.updated_at',
                DB::raw("CONCAT(members.member_first_name,' ',members.member_last_name) AS member_full_name"),
                'member_types.member_type_name'
                )
            ->leftJoin('members','sms.sms_reciever_member_id','=','members.id')
            ->leftJoin('member_types','sms.sms_reciever_member_type','=','member_types.id')
            ->where('sms.division_id',session()->get('member_division_id'))->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm resendSms"> Resend <i class="fa fa-refresh"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        // for getting division member types
        $memberTypes = MemberType::latest()->where('record_hide','NO')->where('division_id',session()->get('member_division_id'))->get();

        return view('admin.sms.manage',compact('memberTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // request()->validate([
        //     'sms_member_tel' => 'required|integer',
        //     'sms_send_message' => 'required|string|min:3',
        //     'sms_reference_type' => 'required|string|min:3',
        //     'sms_member_type_id' => 'required|string|min:1',
        //     'sms_reciever_member_id' => 'required|integer|min:1',
        // ]);

        if (count($request->all()) >= 1 ) {
            //for sending sms and saving in db
            $smsObj = new Sms;
            // sms group number
            $smsGroupNumber = date('dmY').'-'.mt_rand(1,9100);
            // loop through select and send
            foreach($request->sms_reciever_member_id as $memberId){
                $memberDetails = Member::findorFail($memberId);
                $smsObj->send_sms(
                    $memberDetails->member_tel,
                    $request->sms_send_message,
                    $request->sms_reference_type,
                    $smsGroupNumber,
                    $memberDetails->member_type,
                    $memberId
                );
            }

        }

        // 
        return response()->json(['message'=>'successful']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //show the members based on all
        switch (trim($id)) {
            case 0:
               $members  = DB::table('members')->select(
                        'members.id','members.member_tel','members.member_type',
                        DB::raw("CONCAT(members.member_first_name,' ',members.member_last_name) AS member_full_name"),
                        'member_types.member_type_name'
                        )
                        ->leftJoin('member_types','member_types.id','=','members.member_type')
                        ->where('members.division_id',session()->get('member_division_id'))->get();
            break;
            default:
                $members  = DB::table('members')->select(
                    'members.id','members.member_tel','members.member_type',
                    DB::raw("CONCAT(members.member_first_name,' ',members.member_last_name) AS member_full_name"),
                    'member_types.member_type_name'
                    )
                    ->leftJoin('member_types','member_types.id','=','members.member_type')
                    ->where('members.member_type',$id)
                    ->where('members.division_id',session()->get('member_division_id'))->get();
            break;
        }

        // loop through and create checkboxes
        foreach($members as $member){
            $data[] = "<tr>
                            <td><input type='checkbox' name='sms_reciever_member_id[]' class='sms_reciever_member_id' value='$member->id' ></td>
                            <td>$member->member_full_name</td>
                            <td>$member->member_type_name</td>
                            <td>$member->member_tel</td>
                        </tr>";
        }
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
