<?php

namespace App\Http\Controllers\Admin\Groups;

use DataTables;
use App\Admin\Group;
use App\Admin\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            // for selectin groups for divison account
            // $data = Group::latest()->where('record_hide','NO')->where('group_division_id',session()->get('member_division_id'))->get();
            $data = DB::table('groups')->select('groups.id','groups.group_name','groups.updated_at','divisions.division_alias')
            ->leftJoin('divisions','groups.group_division_id','=','divisions.id')
            ->where('groups.record_hide','NO');
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editGroup">Edit <i class="fa fa-edit"></i></a>';
                $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteGroup">Delete <i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        // returning modules for modal
        // $modules = Module::latest()->where('record_hide','NO')->where('module_division_id',session()->get('member_division_id'))->orderBy('module_category','DESC')->get();
        
        $modules = DB::table('modules')->select('modules.id','modules.module_name','modules.module_category','modules.updated_at','divisions.division_alias')
        ->leftJoin('divisions','modules.module_division_id','=','divisions.id')
        ->where('module_division_id',session()->get('member_division_id'))
        ->where('modules.record_hide','NO')->get();
        // get divisions
        $divisions = DB::table('divisions')->select('id','division_alias')->get();

        return view('admin.groups.manage',compact(['modules','divisions']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'group_division_id' => 'required|integer|min:1',
            'group_name' => 'required|string|min:3',
        ]);
        // after validation add the defaults
        $attributes['group_module_id'] = json_encode($request->group_module_id);
        $attributes['updated_member_id'] = session()->get('member_id');
        $attributes['record_hide'] ='NO';
        // switch to save or update
        switch ($request->actionType) {
            case 'create':
                Group::create($attributes);
            break;
            case 'edit':
                Group::findorFail($request->input('group_id'))->update($attributes);
            break;
        }
        return response()->json(['message'=>'successful']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        return response()->json($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // hide record instead of deleting in case is a foreign key in another table
       $attributes['id']= $id;
       $attributes['record_hide']= 'YES';
       $attributes['updated_member_id'] = session()->get('member_id');
       
       Group::findorFail($id)->update($attributes);

       return response()->json(['message'=>'Successful']);
    }
}
