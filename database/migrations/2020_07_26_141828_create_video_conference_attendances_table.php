<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoConferenceAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_conference_attendances', function (Blueprint $table) {
            $table->id();
            $table->integer('attendance_member_type');
            $table->integer('attendance_member_id');
            $table->integer('attendance_conference_id');
            $table->integer('attendance_division_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_conference_attendances');
    }
}
