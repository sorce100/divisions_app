<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoConferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_conferences', function (Blueprint $table) {
            $table->id();
            $table->text('conference_title');
            $table->string('conference_host');
            $table->string('conference_target');
            $table->string('conference_start_date');
            $table->string('conference_end_date');
            $table->text('conference_room_name');
            $table->text('conference_notes');
            $table->integer('division_id');
            $table->integer('updated_member_id');
            $table->string('record_hide');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_conferences');
    }
}
