<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('course_name');
            $table->string('course_alias');
            $table->string('course_year');
            $table->string('course_year_part');
            $table->integer('course_lecturer_member_id');
            $table->string('course_notes')->nullable();
            $table->string('course_folder')->nullable();
            $table->integer('course_division_id');
            $table->integer('updated_member_id');
            $table->string('record_hide');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
