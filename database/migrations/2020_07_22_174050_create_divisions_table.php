<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->id();
            $table->string('division_name');
            $table->string('division_alias');
            $table->string('division_contact_fname');
            $table->string('division_contact_lname');
            $table->string('division_contact_tel');
            $table->text('division_address');
            $table->text('division_location');
            $table->string('division_status_log');
            $table->string('division_logo');
            $table->string('division_folder');
            $table->text('division_notes');
            $table->integer('updated_member_id');
            $table->string('record_hide');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
