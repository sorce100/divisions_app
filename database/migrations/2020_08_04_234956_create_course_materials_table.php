<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_materials', function (Blueprint $table) {
            $table->id();
            $table->integer('course_material_course_id');
            $table->string('course_material_course_folder');
            $table->text('course_material_upload_title');
            $table->string('course_material_target');
            $table->text('course_material_files');
            $table->text('course_material_notes')->nullable();
            $table->integer('updated_member_id');
            $table->integer('course_division_id');
            $table->string('record_hide');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_materials');
    }
}
