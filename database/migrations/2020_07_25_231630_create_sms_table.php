<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->id();
            $table->string('sms_reciever_member_type');
            $table->integer('sms_reciever_member_id');
            $table->string('sms_reference_type');
            $table->string('sms_group_number');
            $table->integer('sms_destination_tel');
            $table->text('sms_send_message');
            $table->text('sms_response');
            $table->integer('division_id');
            $table->integer('sender_member_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
