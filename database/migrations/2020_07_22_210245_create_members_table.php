<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('member_first_name');
            $table->string('member_middle_name')->nullable();
            $table->string('member_last_name');
            $table->string('member_dob');
            $table->integer('member_tel');
            $table->string('member_emergency_tel');
            $table->string('member_email')->unique();
            $table->string('member_region');
            $table->string('member_image');
            $table->string('member_folder');
            $table->string('member_address');
            $table->string('member_location');
            $table->string('member_professional_number');
            $table->string('member_type');
            $table->string('member_designation_id');
            $table->string('member_company_name');
            $table->string('member_company_type');
            $table->string('member_company_tel');
            $table->string('member_company_email');
            $table->string('member_company_region');
            $table->text('member_office_address');
            $table->text('member_office_location');
            $table->text('member_notes');
            $table->string('updated_member_id');
            $table->string('record_hide');
            $table->integer('division_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
