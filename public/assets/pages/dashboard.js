
 $(document).ready(function() {
	
     /*To do list*/
            
     $(".list-task li label").click(function() {
            $(this).toggleClass("task-done");
     });
      /* initialize the calendar
        -----------------------------------------------------------------*/
        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
          },
          defaultDate: '2020-05-20',
          editable: true,
          droppable: true,
          eventLimit: true, 
          events: [
            {
              title: 'Event Meeting 1',
              start: '2020-05-20',
              color: '#FFBD4A'
            },
            {
              title: 'Birthday Party',
             start: '2020-01-21',
              color: '#FC8AB1'
            },
            {
              title: 'Birthday Party',
             start: '2020-01-23',
              color: '#668CFF'
            },
            {
              title: 'Birthday Party',
             start: '2020-05-25',
              color: '#F05050'
            },
            {
              title: 'Birthday Party',
             start: '2020-05-28',
              color: '#81C868'
            },
            {
              title: 'Birthday Party',
             start: '2020-01-30',
              color: '#F05050'
            }
            
            
          ]
        });



    // for checkbox select all
    $('.checkBoxSelectAll').click(function () {
        $('.checkBoxSelect').click();
    });
    
    $('.checkBoxSelect').each(function() {
        $(this).click(function() {
            if($(this).closest('tr').hasClass("checked")){
                $(this).closest('tr').removeClass('checked');
                hiddenMailOptions();
            } else {
                $(this).closest('tr').addClass('checked');
                hiddenMailOptions();
            }
        });
    });


    
});