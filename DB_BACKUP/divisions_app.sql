-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2021 at 11:48 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `divisions_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_year_part` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_lecturer_member_id` int(11) NOT NULL,
  `course_notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_folder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_division_id` int(11) NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_name`, `course_alias`, `course_year`, `course_year_part`, `course_lecturer_member_id`, `course_notes`, `course_folder`, `course_division_id`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(6, 'geodesy', 'geodesy', 'Year 1', 'Part 1', 3, NULL, 'geodesy75', 1, 3, 'NO', '2020-07-29 02:02:27', '2020-07-29 10:06:32'),
(7, 'mapping', 'map', 'Year 1', 'Part 1', 3, NULL, 'map50', 1, 3, 'NO', '2020-07-29 02:03:04', '2020-07-29 12:38:30'),
(8, 'Micheal Nyoagbe', 'MN3', 'Year 1', 'Part 2', 3, NULL, 'MN315', 1, 3, 'NO', '2020-08-11 18:14:02', '2020-08-11 18:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `course_materials`
--

CREATE TABLE `course_materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_material_course_id` int(11) NOT NULL,
  `course_material_course_folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_material_upload_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_material_target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_material_files` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_material_notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_member_id` int(11) NOT NULL,
  `course_division_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_materials`
--

INSERT INTO `course_materials` (`id`, `course_material_course_id`, `course_material_course_folder`, `course_material_upload_title`, `course_material_target`, `course_material_files`, `course_material_notes`, `updated_member_id`, `course_division_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(10, 7, 'map50', 'Best Thing', 'Trainees', '[\"050820200049.pdf\",\"050820200049.pdf\"]', 'First Test', 3, 1, 'NO', '2020-08-05 00:49:29', '2020-08-05 00:49:29'),
(11, 6, 'geodesy75', 'Best Thing', 'Members', '[\"060820200049.docx\",\"050820202358.xlsx\"]', 'None', 3, 1, 'NO', '2020-08-05 16:29:33', '2020-08-06 00:49:25'),
(12, 6, 'geodesy75', 'Best Thing 123', 'Trainees', '[\"06082020007.jpeg\",\"060820200025.pdf\"]', 'fdghjk', 3, 1, 'NO', '2020-08-06 00:25:34', '2020-08-06 01:03:00'),
(13, 6, 'geodesy75', 'For the 8th', 'Members', '[\"080820200121.docx\",\"080820200111.pdf\",\"080820200139.jpeg\"]', 'asdfasdfadsf', 3, 1, 'NO', '2020-08-08 01:45:25', '2020-08-08 01:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `division_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_contact_fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_contact_lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_contact_tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_status_log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `division_name`, `division_alias`, `division_contact_fname`, `division_contact_lname`, `division_contact_tel`, `division_address`, `division_location`, `division_status_log`, `division_logo`, `division_folder`, `division_notes`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(1, 'Ghana Institution Of Surveyors ( LSD )', 'ghislsd', 'micheal', 'nyoagbe', '433454', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'sdfgsdfg', 'ACTIVE', '2008202013.png', 'sdfg', 'sdfgsdfgsdfg', 1, 'NO', '2020-07-22 19:05:46', '2020-08-20 19:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_division_id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_module_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_division_id`, `group_name`, `group_module_id`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(1, 1, 'First Group', '[\"1\",\"5\",\"7\",\"12\",\"13\",\"14\"]', 3, 'NO', '2020-07-31 00:37:31', '2020-08-20 19:08:12'),
(2, 1, 'Second Group', '[\"2\",\"1\"]', 3, 'NO', '2020-07-31 11:39:27', '2020-07-31 23:23:04');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_tel` int(11) NOT NULL,
  `member_emergency_tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_designation_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_company_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_company_tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_company_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_company_region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_office_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_office_location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_professional_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_member_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `member_first_name`, `member_middle_name`, `member_last_name`, `member_dob`, `member_tel`, `member_emergency_tel`, `member_email`, `member_region`, `member_image`, `member_folder`, `member_address`, `member_location`, `member_type`, `member_designation_id`, `member_company_name`, `member_company_type`, `member_company_tel`, `member_company_email`, `member_company_region`, `member_office_address`, `member_office_location`, `member_professional_number`, `member_notes`, `updated_member_id`, `record_hide`, `division_id`, `created_at`, `updated_at`) VALUES
(3, 'sorce', 'kwarteng', 'ogyam', '61993-08-04', 209969656, '123123', 'sorcec100@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'sorcec100@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(4, 'Harry', '', 'Anim', '61993-08-04', 123123, '123123', 'hnanim@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '2', '1', 'fasdfasd', 'civil_service', '234234', 'hnanim@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(5, 'John', '', 'Ayeh', '61993-08-04', 123123, '123123', 'johnnyayer@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'johnnyayer@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(6, 'Samuel', 'Oppong', 'Antwi', '61993-08-04', 123123, '123123', 'samopant@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '2', '1', 'fasdfasd', 'civil_service', '234234', 'samopant@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(7, 'Isaac', '', 'Dadzie ', '61993-08-04', 123123, '123123', 'isaacdadzie@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'isaacdadzie@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(8, 'J', 'C', 'Acquaah ', '61993-08-04', 123123, '123123', 'jcacquaah@yahoo.co.uk', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '2', '1', 'fasdfasd', 'civil_service', '234234', 'jcacquaah@yahoo.co.uk', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(9, 'Tina', '', 'Wamega ', '61993-08-04', 244723459, '0244723459', 'twemeg@yahoo.co.uk', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'twemeg@yahoo.co.uk', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(10, 'Charles', 'Kofi', 'SOM ', '61993-08-04', 241901010, '0241901010', 'kofi.som@statsghana.gov.gh', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'kofi.som@statsghana.gov.gh', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(11, 'Rudolf', 'Kwako Dogbe', 'SOM ', '61993-08-04', 244664446, '244664446', 'rudyexus@yahoo.com;', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'rudyexus@yahoo.com;', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(12, 'Rodney', 'Nii Armah', 'Tagoe  ', '61993-08-04', 541512712, '541512712', 'rudyexus@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'rudyexus@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(13, 'Joshuah', 'Effah', 'Kissi ', '61993-08-04', 273158179, '273158179', 'kissjek@gmail.com ', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'kissjek@gmail.com ', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(14, 'Isaac-Marcel', '', 'Azorliade ', '61993-08-04', 200154757, '200154757', 'isaacazorliade@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'isaacazorliade@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(15, 'Samuel', 'Ofori', 'Arhin ', '61993-08-04', 246361500, '246361500', 'arhinoforisamuel@outlook.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'arhinoforisamuel@outlook.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(16, 'Dennis', 'Boakye', 'Bio ', '61993-08-04', 276007560, '276007560', 'belucci100@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'belucci100@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(17, 'Yakubu', 'Abdul', 'Rahim ', '61993-08-04', 246363500, '246363500', 'yakubuabdulrahim622@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'yakubuabdulrahim622@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(18, 'Basirata', '', 'Abudu-Akindali ', '61993-08-04', 277868629, '277868629', 'basirataabuduabu@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'basirataabuduabu@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(19, 'Simon', 'Suna-Na-Aug', 'Ekumah ', '61993-08-04', 240772976, '240772976', 'winstersim@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'winstersim@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(20, 'Andrew', 'Owuraku', 'Asane ', '61993-08-04', 267802962, '267802962', 'andrewasane@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'andrewasane@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(21, 'Mark', '', 'Adabori ', '61993-08-04', 242531079, '242531079', 'adabormark@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'adabormark@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(22, 'Chairles', 'Tachie', 'Acquah ', '61993-08-04', 243567523, '243567523', 'ct.acquah@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'ct.acquah@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(23, 'Raymond', 'Kingsford', 'Attah ', '61993-08-04', 502481633, '249293708', 'raymondkingsford@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'raymondkingsford@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(24, 'Otchere', 'Bernard', 'Kwaku ', '61993-08-04', 546016345, '546016345', 'blessben16@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'blessben16@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(25, 'Stephen', '', 'Yamoah ', '61993-08-04', 554601782, '554601782', 'silicon95@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'silicon95@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(26, 'Victor', '', 'Quaicoe ', '61993-08-04', 244650461, '244650461', 'quaicovictor@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'quaicovictor@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(27, 'Pius', 'Owusu', 'Achiaw ', '61993-08-04', 249328878, '242978895', 'nanaoachiaw@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'nanaoachiaw@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(28, 'Fosu', '', 'Brempong ', '61993-08-04', 208165508, '208165508', 'obrempongfosu@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'obrempongfosu@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(29, 'Hubert', 'Owusu', 'Boateng ', '61993-08-04', 208165508, '208165508', 'hubertboateng29@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'hubertboateng29@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(30, 'Rebecca', '', 'Dengure ', '61993-08-04', 240822648, '240822648', 'beccadengure@hotmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'beccadengure@hotmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(31, 'Aaron', 'Sika', 'Tei ', '61993-08-04', 245408176, '245408176', 'teisikaaaron@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'teisikaaaron@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(32, 'Adjabeng', 'Michael', 'Dela ', '61993-08-04', 0, '245930019', 'josephnanamintah@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'josephnanamintah@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(33, 'Eli', '', 'Akplor ', '61993-08-04', 248409792, '248409792', 'eliakplor@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'eliakplor@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(34, 'Asante', '', 'Anim ', '61993-08-04', 201095044, '201095044', ' animasante@ymail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', ' animasante@ymail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(35, 'Anthony', 'Tsatsu', 'Onyame ', '61993-08-04', 243267955, '243267955', 'onyameanthony@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'onyameanthony@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(36, 'Cyril', 'Setusa', 'Owiafe ', '61993-08-04', 247680879, '247680879', 'setus249@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'setus249@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(37, 'Jonathan', '', 'Acquaah ', '61993-08-04', 242433011, '242433011', 'gaddielacquaah@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'gaddielacquaah@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(38, 'Alfred', '', 'Batawo ', '61993-08-04', 249815820, '249815820', 'norbertbatawo@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'norbertbatawo@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(39, 'Anku', 'Bright', 'Komla ', '61993-08-04', 246905839, '246905839', 'ankubrightkomla@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'ankubrightkomla@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(40, 'Isaack', 'Kobina', 'Gyasi-Addo ', '61993-08-04', 277076060, '277076060', 'gisaackobina@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'gisaackobina@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(41, 'Emmanuel', '', 'Ofotsu ', '61993-08-04', 540954100, '540954100', 'emmanuelofotsu@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'emmanuelofotsu@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(42, 'Simba', '', 'Bright ', '61993-08-04', 249269544, '249269544', 'brightdiggy@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'brightdiggy@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(43, 'Opoku', 'Prince', 'Kwabena ', '61993-08-04', 243183183, '243183183', 'positive_prince@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'positive_prince@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(44, 'Afua', 'Antwiwaa', 'Abasa ', '61993-08-04', 206368676, '206368676', 'afuaabasa@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'afuaabasa@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(45, 'Ebenezer', '', 'Abudu ', '61993-08-04', 500014927, '500014927', 'abudu.ebenezer@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'abudu.ebenezer@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(46, 'Eric', '', 'Otoo ', '61993-08-04', 244922573, '244922573', 'paakowotoo@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'paakowotoo@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(47, 'Joel', 'Kwabena', 'Lawerteh ', '61993-08-04', 263261364, '263261364', 'lawerteh@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'lawerteh@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(48, 'Emmanuel', '', 'Boateng ', '61993-08-04', 242129204, '242129204', 'em3boateng@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'em3boateng@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(49, 'Vincent', 'Anim', 'Tsawo ', '61993-08-04', 540839956, '540839956', '540839956', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', '540839956', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(50, 'Geoffery', 'Kwaku', 'Eto ', '61993-08-04', 244031208, '244031208', 'geofferyeto@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'geofferyeto@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(51, 'Bismark', '', 'Oteng ', '61993-08-04', 244589905, '244589905', 'bismarkoteng98@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'bismarkoteng98@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(52, 'Edward', '', 'Ansah ', '61993-08-04', 549154680, '549154680', 'edwardumat@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'edwardumat@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(53, 'Joseph', 'Tetteh', 'Sackey ', '61993-08-04', 244839723, '244839723', 'akpesac@yahoo.co.uk', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'akpesac@yahoo.co.uk', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(54, 'Dzaka', 'Emmanuel', 'Kojo ', '61993-08-04', 207515843, '207515843', 'yesukodzaka@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'yesukodzaka@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(55, 'Osman', 'Abdul', 'Malik ', '61993-08-04', 249824361, '249824361', 'yamalik1986@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'yamalik1986@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(56, 'Burma', 'Abubakar', 'Abdul-Karim ', '61993-08-04', 249005287, '249005287', 'burma202@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'burma202@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(57, 'Esther', '', 'Opoku-Agyemang ', '61993-08-04', 571998152, '571998152', 'estheroa8@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'estheroa8@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(58, 'Joseph', 'Tetteh', 'Sackey ', '61993-08-04', 244839723, '244839723', 'ajoesac@yahoo.co.uk ', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'ajoesac@yahoo.co.uk ', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(59, 'Atta', 'Boakye', 'Boateng ', '61993-08-04', 207229626, '207229626', 'boateng.attaboakye.abb@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'boateng.attaboakye.abb@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(60, 'Enoch', '', 'Osei ', '61993-08-04', 242555398, '242555398', 'enochosei52@hotmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'enochosei52@hotmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(61, 'Peter', '', 'Yankey ', '61993-08-04', 243945464, '243945464', 'peteryankeyjnr@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'peteryankeyjnr@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(62, 'Ziblim', '', 'Alhassan ', '61993-08-04', 209779720, '209779720', 'ziblimalhassan@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'ziblimalhassan@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(63, 'Owusu', 'Sekyere', 'Abel ', '61993-08-04', 543345701, '543345701', 'abelsekyere2@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'abelsekyere2@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(64, 'Prosper', 'Akorfanu', 'sabi ', '61993-08-04', 540958994, '540958994', 'sabiprosper@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'sabiprosper@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(65, 'Mutaru', 'Adam', 'Fuseini ', '61993-08-04', 545566875, '545566875', 'mufuza@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'mufuza@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(66, 'Adombila', 'Ayamga', 'Francis ', '61993-08-04', 245370805, '245370805', 'frankadom@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'frankadom@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(67, 'Benjamin', '', 'Adu ', '61993-08-04', 544242570, '544242570', 'adukofibenjamin@gmail.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'adukofibenjamin@gmail.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(68, 'Yahaya', '', 'Mutaru ', '61993-08-04', 249196425, '249196425', 'y.mutaru@yahoo.com', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'y.mutaru@yahoo.com', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(69, 'Abdulai', '', 'Fuseini ', '61993-08-04', 244179286, '244179286', 'fuzian.fuzian@yahoo.com ', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'fuzian.fuzian@yahoo.com ', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(70, 'Elias', 'Kpadas', 'Ismael ', '61993-08-04', 244050127, '244050127', 'eismael@icloud.com ', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'eismael@icloud.com ', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(71, 'Prosper', 'Kwame', 'Detornu ', '61993-08-04', 208774401, '208774401', 'femiprozey@yahoo.com ', 'Bono_East', '2207202036.jpg', 'ogyam99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdffffffffffffffffff', '1', '1', 'fasdfasd', 'civil_service', '234234', 'femiprozey@yahoo.com ', 'Western_North', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '232423', 'asdfasdfasdfsdaf', '1', 'NO', 1, '2020-07-22 23:36:34', '2020-07-22 23:36:34'),
(72, 'asdf', 'asdfasd', 'asdf', '29-07-2020', 24324, '324234', 'asdf@gmail.com', 'Bono_East', '2907202059.jpg', 'asdf99', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '1', '1', 'asdfasdf', 'public_service', '2342343', 'asdf@gmail.com', 'Western', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfasdf', '2423423', 'asdfasdfsdf', '1', 'NO', 1, '2020-07-29 11:59:59', '2020-07-29 11:59:59'),
(73, 'asdfasd', 'asfasdf', 'asdfasdf', '29-07-2020', 209969656, '0209969656', 'merlin@gmail.com', 'Central', '2907202057.jpg', 'asdfasdf63', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdf 123', '1', '1', 'asfasdf', 'public_service', '324234', 'sdfas@gmail.com', 'Western', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdfsadf', '23423', 'asdfasdf', '1', 'NO', 1, '2020-07-29 12:07:21', '2020-07-29 12:57:20'),
(74, 'Testing 123', NULL, 'Testing 123', '29-07-2020', 263591061, '0209969656', 's100@gmail.com', 'Central', '0908202052.jpg', 'Testing61', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'Adenta', '1', '1', 'adenta main', 'civil_service', '209969655', 'sorce100@gmail.com', 'Western', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'Athens@123', '2345', 'None 123', '3', 'NO', 1, '2020-07-29 12:59:19', '2020-08-19 14:11:39'),
(283, 'Hebert', 'Kwadwo', 'Otchere', '18-08-2020', 206677883, '0206677883', 'herbet@gmail.com', 'Eastern', 'NONE', 'NONE', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdf', '7', '2', 'none', 'Others', '0206677883', 'herbert@gmail.com', 'Greater_Accra', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'asdasdfa', '123456', 'asdadfasdf', '3', 'NO', 1, '2020-08-11 23:56:27', '2020-08-11 23:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `member_designations`
--

CREATE TABLE `member_designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_designation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_designation_notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member_designations`
--

INSERT INTO `member_designations` (`id`, `member_designation_name`, `member_designation_notes`, `division_id`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(1, 'FGhIS', 'NONE', 1, 3, 'NO', '2020-08-10 00:49:32', '2020-08-10 00:49:32'),
(2, 'MGhIS', 'NONE', 1, 3, 'NO', '2020-08-10 00:50:30', '2020-08-10 00:50:30'),
(3, 'TechGhIS', 'NONE', 1, 3, 'NO', '2020-08-10 00:50:57', '2020-08-10 00:50:57'),
(4, 'Others', 'NONE', 1, 3, 'NO', '2020-08-10 00:51:09', '2020-08-10 01:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `member_types`
--

CREATE TABLE `member_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_type_notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member_types`
--

INSERT INTO `member_types` (`id`, `member_type_name`, `member_type_notes`, `division_id`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(1, 'Trainee', 'none', 1, 3, 'NO', '2020-08-10 00:15:27', '2020-08-10 00:18:58'),
(2, 'Fellow', 'None', 1, 3, 'NO', '2020-08-10 00:16:34', '2020-08-10 00:16:34'),
(3, 'Professional', 'None', 1, 3, 'NO', '2020-08-10 00:16:44', '2020-08-10 00:16:44'),
(4, 'Probationer', 'None', 1, 3, 'NO', '2020-08-10 00:17:48', '2020-08-10 00:17:48'),
(5, 'Technician', 'None', 1, 3, 'NO', '2020-08-10 00:18:02', '2020-08-10 00:18:02'),
(6, 'Licensed', 'None', 1, 3, 'NO', '2020-08-10 00:18:17', '2020-08-10 00:18:17'),
(7, 'Other', 'None', 1, 3, 'NO', '2020-08-10 00:18:29', '2020-08-10 00:18:29');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_07_20_214038_create_modules_table', 1),
(10, '2020_07_22_174050_create_divisions_table', 3),
(11, '2020_07_22_210245_create_members_table', 4),
(12, '2014_10_12_000000_create_users_table', 5),
(13, '2020_07_23_112601_create_video_conferences_table', 6),
(17, '2020_07_26_141828_create_video_conference_attendances_table', 8),
(18, '2016_09_12_99999_create_visitlogs_table', 9),
(22, '2020_07_28_114709_create_courses_table', 10),
(23, '2020_07_22_124756_create_groups_table', 11),
(25, '2020_08_04_234956_create_course_materials_table', 12),
(27, '2020_08_09_224753_create_member_types_table', 13),
(28, '2020_08_10_004152_create_member_designations_table', 14),
(29, '2020_07_25_231630_create_sms_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_division_id` int(11) NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_name`, `module_category`, `module_url`, `module_division_id`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(1, 'Module and Group', 'Administrator', '<li class=\"menu-list\">\r\n                  <a href=\"#\"><i class=\"fa fa-cogs\"></i> <span>Settings</span></a>\r\n                  <ul class=\"sub-menu-list\">\r\n                     <li><a href=\"/Admin/group_manage\"><span>Groups</span></a></li>\r\n                     <li><a href=\"/Admin/module_manage\"><span>Module</span></a></li>\r\n                     \r\n                  </ul>\r\n               </li>', 1, 1, 'NO', '2020-07-22 12:44:30', '2020-08-07 10:57:43'),
(2, 'Member Conference Dashboard', 'Member', '<li><a href=\"/Member/conference_dashboard\"><i class=\"fa fa-desktop\"></i> <span>Video Conference</span></a></li>', 1, 1, 'NO', '2020-07-22 12:46:23', '2020-08-07 10:35:01'),
(5, 'Setup Video Conference', 'Member', '<li><a href=\"/Admin/conference_manage\"><i class=\"fa fa-cogs\"></i> <span>Setup Video Conference </span></a></li>', 1, 1, 'NO', '2020-08-07 10:47:18', '2020-08-07 10:47:18'),
(6, 'Trainees Module', 'Trainee', '<li class=\"menu-list\">\r\n                     <a href=\"#\"><i class=\"fa fa-graduation-cap\"></i> <span>Trainee Modules</span></a>\r\n                     <ul class=\"sub-menu-list\">\r\n                        <li><a href=\"/Trainee/dashboard\"> Trainee Dashboard </a></li>\r\n                        <li><a href=\"/Trainee/trainee_conference\"> Class Rooms </a></li>\r\n                        <li><a href=\"/Trainee/profile\"> Profile </a></li>\r\n                        <li><a href=\"/Trainee/results\"> Results </a></li>\r\n                        <li><a href=\"/Trainee/registration\"> Registration </a></li>\r\n                        <li><a href=\"/Trainee/fees\"> Fees </a></li>\r\n                        <li><a href=\"/Trainee/course_materials\"> Course Material </a></li>\r\n                        <li><a href=\"/Trainee/lecturer_assessment\"> Lecturer Assessment </a></li>\r\n                     </ul>\r\n                  </li>', 1, 1, 'NO', '2020-08-07 10:52:59', '2020-08-07 10:52:59'),
(7, 'SMS', 'Administrator', '<li><a href=\"/Admin/sms_manage\"><i class=\"fa fa-comments\"></i> <span>SMS </span></a></li>', 1, 1, 'NO', '2020-08-07 10:58:15', '2020-08-07 10:58:15'),
(8, 'Trainee Lecturers', 'Member', '<li class=\"menu-list\">\r\n                  <a href=\"#\"><i class=\"fa fa-users\"></i> <span> Trainee Lecturers </span></a>\r\n                  <ul class=\"sub-menu-list\">\r\n                     <li><a href=\"/Admin/lecturer_dashboard\">  Dashboard </a></li>\r\n                     <li><a href=\"/Admin/lecturer_all_trainees\">  All Trainees  </a></li>\r\n                     <li><a href=\"/Admin/lecturer_course_material\"> Upload Course Material </a></li>\r\n                     <li><a href=\"/Member/conference_dashboard\"> Class Rooms </a></li>\r\n                     <li><a href=\"/Admin/lecturer_attendance\"> Class Rooms Attendance </a></li>\r\n                     <li><a href=\"/Admin/lecturer_exams_result\"> Trainee Exams Records </a></li>\r\n                  </ul>\r\n               </li>', 1, 1, 'NO', '2020-08-07 11:56:05', '2020-08-07 11:56:05'),
(9, 'Trainees Moderator', 'Member', '<li class=\"menu-list\">\r\n                  <a href=\"#\"><i class=\"fa fa-cogs\"></i> <span> Moderator Trainees </span></a>\r\n                  <ul class=\"sub-menu-list\">\r\n                     <li><a href=\"/Admin/moderator_dashboard\">  Dashboard </a></li>\r\n                     <li><a href=\"/Admin/moderator_setup_course\"> Setup Courses </a></li>\r\n                     <li><a href=\"/Admin/moderator_publish_result\"> Publish Results </a></li>\r\n                     <li><a href=\"/Admin/moderator_video_conference\"> Moderator Class Room </a></li>\r\n                     <li><a href=\"/Admin/moderator_manage_trainee\"> Manage Trainees </a></li>\r\n                     <li><a href=\"/Admin/moderator_setup_calendar\"> Setup Calendar </a></li>\r\n                     <li><a href=\"/Admin/moderator_lecturer_assement\"> View Lecturers Assessment </a></li>\r\n                  </ul>\r\n               </li>', 1, 1, 'NO', '2020-08-07 12:00:56', '2020-08-07 12:00:56'),
(10, 'Member Messages', 'Member', '<li><a href=\"/Member/message_dashboard\"><i class=\"fa fa-envelope-o\"></i> <span>Messages</span></a></li>', 1, 1, 'NO', '2020-08-07 12:04:26', '2020-08-07 12:04:26'),
(11, 'Member Library Archive', 'Member', '<li><a href=\"/Member/library_dashboard\"><i class=\"fa fa-book\"></i> <span>Library Archive</span></a></li>', 1, 1, 'NO', '2020-08-07 12:04:52', '2020-08-07 12:04:52'),
(12, 'Member Setup', 'Administrator', '<li class=\"menu-list\">\r\n                  <a href=\"#\"><i class=\"fa fa-users\"></i> <span>Member</span></a>\r\n                  <ul class=\"sub-menu-list\">\r\n                     <li><a href=\"/Admin/member_dashboard\"><span>Dashboard</span></a></li>\r\n                     <li><a href=\"/Admin/member_manage\"><span>Manage</span></a></li>\r\n                     <li><a href=\"/Admin/member_type\"><span>Type</span></a></li>\r\n                     <li><a href=\"/Admin/member_designation\"><span>Designation</span></a></li>\r\n                     <li><a href=\"/Admin/member_report\"><span>Report</span></a></li>\r\n                     \r\n                  </ul>\r\n               </li>', 1, 1, 'NO', '2020-08-07 12:09:16', '2020-08-07 12:09:16'),
(13, 'Division Setup', 'Administrator', '<li class=\"menu-list\">\r\n                  <a href=\"#\"><i class=\"fa fa-cogs\"></i> <span>Setup Division</span></a>\r\n                  <ul class=\"sub-menu-list\">\r\n                     <li><a href=\"/Admin/division_dashboard\"> Divisions Dashboard </a></li>\r\n                     <li><a href=\"/Admin/division_manage\"> Manage Division </a></li>\r\n                     <li><a href=\"/Admin/division_account\"> Division Accounts </a></li>\r\n                     <li><a href=\"/Admin/division_report\"> Division Reports </a></li>\r\n                     \r\n                  </ul>\r\n               </li>', 1, 1, 'NO', '2020-08-07 12:17:00', '2020-08-07 12:17:00'),
(14, 'Users Setup', 'Administrator', '<li class=\"menu-list\">\r\n                  <a href=\"#\"><i class=\"fa fa-users\"></i> <span>Users</span></a>\r\n                  <ul class=\"sub-menu-list\">\r\n                     <li><a href=\"/Admin/users_dashboard\"> Users Dashboard </a></li>\r\n                     <li><a href=\"/Admin/users_manage\"> Manage User </a></li>\r\n                     <li><a href=\"/Admin/users_sessions\"> Users Sessions </a></li>\r\n                     <li><a href=\"/Admin/users_report\"> Users Reports </a></li>\r\n                  </ul>\r\n               </li>', 1, 1, 'NO', '2020-08-07 12:19:06', '2020-08-07 12:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sms_reciever_member_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_reciever_member_id` int(11) NOT NULL,
  `sms_reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_group_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_destination_tel` int(11) NOT NULL,
  `sms_send_message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_response` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `sender_member_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `sms_reciever_member_type`, `sms_reciever_member_id`, `sms_reference_type`, `sms_group_number`, `sms_destination_tel`, `sms_send_message`, `sms_response`, `division_id`, `sender_member_id`, `created_at`, `updated_at`) VALUES
(1, '1', 74, 'Single', '11082020-76', 209969656, 'Dear Testing 123 Testing 123, your GhIS Member account has being created successfully. Please log on https://ghislsd.com with credentials, email: s100@gmail.com and password : s100', 'Operation successful.', 1, 3, '2020-08-11 00:09:30', '2020-08-11 00:09:30'),
(2, '1', 3, 'Bulk', '11082020-76', 209969656, 'Testing Bulk', 'Operation successful.', 1, 3, '2020-08-11 18:22:02', '2020-08-11 18:22:02'),
(3, '1', 3, 'Bulk', '11082020-7322', 209969656, 'Bulk testing', 'Operation successful.', 1, 3, '2020-08-11 23:47:09', '2020-08-11 23:47:09'),
(4, '1', 74, 'Bulk', '11082020-7322', 123123, 'Bulk testing', 'Operation successful.', 1, 3, '2020-08-11 23:47:10', '2020-08-11 23:47:10'),
(5, '7', 283, 'Bulk', '11082020-515', 206677883, 'Dear Herbert Otchere , Congratulations,Your application for the position of .... kwasia, do den bed.', 'Operation successful.', 1, 3, '2020-08-11 23:58:34', '2020-08-11 23:58:34'),
(6, '1', 74, 'Bulk', '14082020-1522', 209969656, 'Testing', 'Operation successful.', 1, 3, '2020-08-14 19:37:23', '2020-08-14 19:37:23'),
(7, '1', 73, 'Bulk', '14082020-7622', 209969656, 'This is for testing', 'Operation successful.', 1, 3, '2020-08-14 19:39:38', '2020-08-14 19:39:38'),
(8, 'Member', 74, 'Single', '19082020-2733', 263591061, 'Dear Testing 123 Testing 123, your GhIS Member account has being created successfully. Please log on https://ghislsd.com with credentials, email: s100@gmail.com and password : s100', 'Operation successful.', 1, 3, '2020-08-19 14:13:42', '2020-08-19 14:13:42'),
(9, 'Member', 74, 'Single', '19082020-1504', 263591061, 'Dear Testing 123 Testing 123, your GhIS Member account has being created successfully. Please log on https://ghislsd.com with credentials, email: s100@gmail.com and password : s100', 'Operation successful.', 1, 3, '2020-08-19 14:20:23', '2020-08-19 14:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_division_id` int(11) NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_member_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `user_login_status_log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_account_status_log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_division_id`, `user_type`, `user_member_id`, `email`, `email_verified_at`, `password`, `user_group_id`, `user_login_status_log`, `user_account_status_log`, `user_notes`, `updated_member_id`, `record_hide`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Trainee', 3, 'sorce100@gmail.com', NULL, '$2y$10$Xt2L.vV2s83hpUQlIu4aSO0oKAne80HFzuFOmSfwYlPoqO1rofmsG', 1, 'LOGIN', 'ACTIVE', 'asdfasdf', 1, 'NO', NULL, '2020-07-23 06:15:55', '2020-07-23 06:15:55'),
(2, 1, 'Member', 3, 'merlin@gmail.com', NULL, '$2y$10$fySQ9JnKFJh.LeG0V62TJ..dtpvtU9xpc0l/mQX5sabsI4I2rr1tS', 1, 'LOGIN', 'ACTIVE', 'asdf', 1, 'NO', NULL, '2020-07-23 08:08:41', '2020-07-23 08:08:41'),
(3, 1, 'Member', 4, 'hnanim@gmail.com', NULL, '$2y$10$IoGzz90k6k6GN1Vf6yNaZ.goMzjf23f3ZMgU7Srgs70BY2KdR4kNu', 1, 'LOGIN', 'ACTIVE', 'none', 1, 'NO', NULL, '2020-07-24 12:55:40', '2020-07-24 12:55:40'),
(4, 1, 'Member', 5, 'johnnyayer@gmail.com', NULL, '$2y$10$hMQMRneiLoXcGyQZPJq1OePwOCkcBK6dducxjN/nIrtOPk19dmeAy', 1, 'LOGIN', 'ACTIVE', 'none', 1, 'NO', NULL, '2020-07-24 12:56:42', '2020-07-24 12:56:42'),
(5, 1, 'Member', 6, 'samopant@yahoo.com', NULL, '$2y$10$zSONS5Vxnu27uIGYZp0q6uvXwM9Xl5sXGg8WSkjGs/ooCpdcaMrgG', 1, 'LOGIN', 'ACTIVE', 'none', 1, 'NO', NULL, '2020-07-24 12:57:15', '2020-07-24 12:57:15'),
(6, 1, 'Member', 7, 'isaacdadzie@yahoo.com', NULL, '$2y$10$prNre8I.SDqyGPbTfBc3ue.fuPkYSX6ESm.MPCx1NbymAbOcemYhi', 1, 'LOGIN', 'ACTIVE', 'none', 1, 'NO', NULL, '2020-07-24 12:57:42', '2020-07-24 12:57:42'),
(7, 1, 'Member', 8, 'jcacquaah@yahoo.co.uk', NULL, '$2y$10$8oIbW91ygIh18cKZCUUb3ea8odfzYpsdN.61Tr9l.JivTBEjC7Rs2', 1, 'LOGIN', 'ACTIVE', 'none', 1, 'NO', NULL, '2020-07-24 12:58:15', '2020-07-24 12:58:15'),
(13, 1, 'Trainee', 3, 'anita@gmail.com', NULL, '$2y$10$AkJsRbzvCZxo/YAQyUQHiOyoIa3N.5pM160sDuIpXxHXrvbhxvtBi', 1, 'LOGIN', 'ACTIVE', 'none', 3, 'NO', NULL, '2020-07-24 19:59:29', '2020-07-24 19:59:29'),
(14, 1, 'Trainee', 3, 'sorcec100@gmail.com', NULL, '$2y$10$88NS371TjJDE04y.4bRLoOO/nBeV6Hgx7TEqR3oP8ZOjS7xliDx.q', 1, 'LOGIN', 'ACTIVE', 'None', 3, 'NO', NULL, '2020-07-26 14:10:33', '2020-07-26 14:10:33'),
(17, 1, 'Member', 74, 's100@gmail.com', NULL, '$2y$10$CiR4QDgcr8roSoujYf27BOOFAo4hUlqzBnTCC6Y2h6hrBPPPhckFa', 1, 'LOGIN', 'ACTIVE', 'asdfasdfkasdfasdf', 3, 'NO', NULL, '2020-08-19 14:20:23', '2020-08-19 14:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `video_conferences`
--

CREATE TABLE `video_conferences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `conference_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference_host` varchar(240) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference_target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference_start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference_end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference_room_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `updated_member_id` int(11) NOT NULL,
  `record_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video_conferences`
--

INSERT INTO `video_conferences` (`id`, `conference_title`, `conference_host`, `conference_target`, `conference_start_date`, `conference_end_date`, `conference_room_name`, `conference_notes`, `division_id`, `updated_member_id`, `record_hide`, `created_at`, `updated_at`) VALUES
(1, 'first test 123', 'Micheal Nyoagbe 123', 'Members', '23-07-2020 | 11:10 AM', '09-07-2020 | 08:00 AM', 'GhISLSD', 'asdfasdfasdfasdfasd', 1, 3, 'NO', '2020-07-23 11:52:10', '2020-07-23 12:39:57'),
(2, 'LISAG GENERAL MEETING', 'MICHEAL NYOAGBE', 'Members', '23-07-2020 | 13:10 PM', '25-07-2020 | 13:35 PM', 'Lisag_SouthernSector', 'None', 1, 1, 'YES', '2020-07-23 13:36:55', '2020-07-23 13:39:05'),
(3, 'For Trainees', 'Sorce Kwarteng', 'Trainees', '23-07-2020 | 06:05 AM', '24-07-2020 | 02:05 AM', 'ghislsd', 'None', 1, 3, 'NO', '2020-07-23 22:06:18', '2020-07-23 22:06:18'),
(4, 'Trainees Moderator and Lectures Room', 'Trainees Moderator', 'Moderators', '24-07-2020 | 17:05 PM', '24-07-2020 | 11:55 AM', 'GhisLsdModerators', 'None', 1, 3, 'NO', '2020-07-24 17:10:30', '2020-07-24 17:10:30'),
(5, 'The land Surveyor and the mineral cadastral administration system and licensing in Ghana', 'Surv.Kwadwo Apraku Wiredu', 'Guest', '20-08-2020 | 13:00 PM', '20-08-2020 | 16:00 PM', 'ghislsd_cpd_guest', 'NONE', 1, 3, 'NO', '2020-08-14 10:43:37', '2020-08-14 10:43:37');

-- --------------------------------------------------------

--
-- Table structure for table `video_conference_attendances`
--

CREATE TABLE `video_conference_attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_member_type` int(11) NOT NULL,
  `attendance_member_id` int(11) NOT NULL,
  `attendance_conference_id` int(11) NOT NULL,
  `attendance_division_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video_conference_attendances`
--

INSERT INTO `video_conference_attendances` (`id`, `attendance_member_type`, `attendance_member_id`, `attendance_conference_id`, `attendance_division_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 3, 1, '2020-07-26 20:19:26', '2020-07-26 20:19:26'),
(2, 1, 3, 3, 1, '2020-07-27 00:40:51', '2020-07-27 00:40:51'),
(3, 1, 3, 5, 1, '2020-08-14 11:14:20', '2020-08-14 11:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `visitlogs`
--

CREATE TABLE `visitlogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.0.0.0',
  `browser` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitlogs`
--

INSERT INTO `visitlogs` (`id`, `ip`, `browser`, `os`, `user_id`, `user_name`, `country_code`, `country_name`, `region_name`, `city`, `zip_code`, `time_zone`, `latitude`, `longitude`, `is_banned`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Chrome (84.0.4147.125)', 'Windows', '0', 'Guest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-07-28 02:37:52', '2020-08-22 08:27:23'),
(2, '0.0.0.0', 'unknown (unknown)', 'unknown', '0', 'Guest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-09 00:34:36', '2020-08-09 00:34:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_materials`
--
ALTER TABLE `course_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_member_email_unique` (`member_email`);

--
-- Indexes for table `member_designations`
--
ALTER TABLE `member_designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_types`
--
ALTER TABLE `member_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video_conferences`
--
ALTER TABLE `video_conferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_conference_attendances`
--
ALTER TABLE `video_conference_attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitlogs`
--
ALTER TABLE `visitlogs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `course_materials`
--
ALTER TABLE `course_materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;

--
-- AUTO_INCREMENT for table `member_designations`
--
ALTER TABLE `member_designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `member_types`
--
ALTER TABLE `member_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `video_conferences`
--
ALTER TABLE `video_conferences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `video_conference_attendances`
--
ALTER TABLE `video_conference_attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `visitlogs`
--
ALTER TABLE `visitlogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
